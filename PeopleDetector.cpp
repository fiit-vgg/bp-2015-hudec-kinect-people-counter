#include "stdafx.h"
#include "PeopleDetector.h"

PeopleDetector::PeopleDetector(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);

	m_pStreamCheckerGroup = new QActionGroup(this);
	ui.actionFrom_File->setActionGroup(m_pStreamCheckerGroup);
	ui.actionFrom_Kinect->setActionGroup(m_pStreamCheckerGroup);
	ui.outputPath->setText(QDir::currentPath());

	m_pSegmentationMethodGroup = new QActionGroup(this);
	ui.actionReconstruction->setActionGroup(m_pSegmentationMethodGroup);
	ui.actionThreshold->setActionGroup(m_pSegmentationMethodGroup);
	ui.actionThreshold_OTSU->setActionGroup(m_pSegmentationMethodGroup);
	ui.actionThreshold_Adaptive->setActionGroup(m_pSegmentationMethodGroup);

	m_pTrackingMethodGroup = new QActionGroup(this);
	ui.actionLucas_Kanade->setActionGroup(m_pTrackingMethodGroup);
	ui.actionFarneback->setActionGroup(m_pTrackingMethodGroup);
	ui.actionSimpleBlob->setActionGroup(m_pTrackingMethodGroup);

	QObject::connect(&m_DetectionManager,&DetectionManager::imagesLoaded,this,&PeopleDetector::showImages);
	QObject::connect(&m_DetectionManager,&DetectionManager::statusChanged,this,&PeopleDetector::fileStreamStatus);
	QObject::connect(&m_PitchDialog,&KinectPitch::valueChanged,this,&PeopleDetector::setKinectPitch);
	QObject::connect(&m_CounterSettingsDialog,&PeopleCounterSettings::doorLinePositionChanged,&(m_DetectionManager.m_PeopleCounter),&PeopleCounter::changeDoorLinePosition);
	QObject::connect(&m_CounterSettingsDialog,&PeopleCounterSettings::areaLinePositionChanged,&(m_DetectionManager.m_PeopleCounter),&PeopleCounter::changeAreaLinePosition);
	QObject::connect(&m_CounterSettingsDialog,&PeopleCounterSettings::orientationChanged,&(m_DetectionManager.m_PeopleCounter),&PeopleCounter::changeOrientation);
	QObject::connect(&m_CounterSettingsDialog,&PeopleCounterSettings::initialPeopleInRoomSet,&(m_DetectionManager.m_PeopleCounter),&PeopleCounter::SetInRoomCounter);
	QObject::connect(&(m_DetectionManager.m_PeopleCounter),&PeopleCounter::peopleMovementNoted,&m_FileManager,&FileStreamManager::logRoomMovement,Qt::ConnectionType::DirectConnection);

	m_DetectionManager.SetGuiMutex(&guiMutex);
	m_DetectionManager.SetFileStreamManager(&m_FileManager);
}

PeopleDetector::~PeopleDetector()
{
	m_DetectionManager.terminate();
}

void PeopleDetector::on_openFilePshBtn_clicked()
{
	QString directory = QFileDialog::getExistingDirectory(new QWidget(), tr("Select output directory"), ui.outputPath->displayText(), QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
	if (!directory.isEmpty())
	{
		ui.outputPath->setText(directory);
		m_FileManager.SetOutputDirectory(directory.toStdString());
	}
}

void PeopleDetector::on_playPshBtn_clicked()
{
	if (ui.playPshBtn->isChecked())
	{
		if (ui.actionFrom_Kinect->isChecked())
		{
			m_KinectController.OpenStream();
		}
		m_DetectionManager.start();
		ui.actionFrom_File->setDisabled(true);
		ui.actionFrom_Kinect->setDisabled(true);
		ui.statusBar->showMessage(QString("Stream started"),2000);
	}
	else
	{
		m_DetectionManager.StopThread();
		m_KinectController.StopStream();
		ui.actionFrom_File->setDisabled(false);
		ui.actionFrom_Kinect->setDisabled(false);
		ui.statusBar->showMessage(QString("Stream paused"));
	}
}

void PeopleDetector::on_recordPshBtn_clicked()
{
	if (ui.recordPshBtn->isChecked())
	{
		QString directory = ui.outputPath->displayText();
		if (!directory.isEmpty())
		{
			m_FileManager.SetOutputDirectory(directory.toStdString());
			ui.outputPath->setDisabled(true);
			ui.openFilePshBtn->setDisabled(true);
			
			m_FileManager.StartRecording();
		}
		else
		{
			ui.statusBar->showMessage("Error - Wrong path", 5000);
			ui.recordPshBtn->setChecked(false);
		}

	}
	else 
	{
		ui.outputPath->setDisabled(false);
		ui.openFilePshBtn->setDisabled(false);
		m_FileManager.StopRecording();
	}

}

void PeopleDetector::on_detectPshBtn_clicked()
{
	if (ui.detectPshBtn->isChecked())
	{
		ui.threshVal->setDisabled(true);
		if (ui.actionThreshold->isChecked())
		{
			m_DetectionManager.SetSegmentationMethod(DetectionManager::SegmentationMethod::THRESHOLD);
		}
		m_DetectionManager.SetThresholdValue(ui.threshVal->value());
		m_DetectionManager.SetMinimalHumanHeightToTrack(ui.threshVal->value());
		HRESULT hr = m_FileManager.OpenLogFile();
		m_DetectionManager.DetectionStatus(true);
		if (hr == ERROR_ALREADY_EXISTS)
		{
			ui.statusBar->showMessage("Existing file opened to write data");
		}
		if (hr == S_OK) 
		{
			ui.statusBar->showMessage("New file created to write data");
		}
		if (hr == ERROR_INVALID_HANDLE)
		{
			ui.statusBar->showMessage("Internal problem, unable to open file");
		}

	}
	else
	{
		ui.threshVal->setDisabled(false);
		m_FileManager.CloseLogFile();
		m_DetectionManager.DetectionStatus(false);
	}
}

void PeopleDetector::exitProgram()
{
	m_DetectionManager.StopThread();
	exit(0);
}

void PeopleDetector::connectKinect()
{
	HRESULT hr = m_KinectController.InitializeFirstConnected();
	if (hr == S_OK)
	{
		ui.playPshBtn->setDisabled(false);
		ui.recordPshBtn->setDisabled(false);
		ui.detectPshBtn->setDisabled(false);
		ui.openFilePshBtn->setDisabled(false);
		ui.outputPath->setDisabled(false);
		ui.actionConnect->setDisabled(true);
		ui.actionChange_Pitch->setDisabled(false);
		m_DetectionManager.SetDataStreamManager(m_KinectController.GetKinectDataStreamManager());
		ui.statusBar->showMessage(QString("Kinect succesfully connected"),3000);
	}
	else
	{
		ui.statusBar->showMessage(QString("Unspecified Error while connecting to Kinect"));
	}

}

void PeopleDetector::changeKinectPitch()
{
	long degree;
	m_KinectController.GetAngle(&degree);
	m_PitchDialog.SetActualAngle(degree);
	m_PitchDialog.exec();
}

void PeopleDetector::kinectStreamActivate()
{
	ui.actionConnect->setDisabled(false);
	ui.actionSource_folder->setDisabled(true);
	ui.actionOutput_folder->setDisabled(true);
	if (m_KinectController.IsInitialized())
	{
		ui.playPshBtn->setDisabled(false);
		ui.recordPshBtn->setDisabled(false);
		ui.detectPshBtn->setDisabled(false);
		ui.openFilePshBtn->setDisabled(false);
		ui.outputPath->setDisabled(false);
		ui.actionConnect->setDisabled(true);
		ui.threshVal->setDisabled(false);
	}
	else
	{
		ui.playPshBtn->setDisabled(true);
		ui.recordPshBtn->setDisabled(true);
		ui.detectPshBtn->setDisabled(true);
		ui.openFilePshBtn->setDisabled(true);
		ui.outputPath->setDisabled(true);
		ui.actionConnect->setDisabled(false);
		ui.threshVal->setDisabled(true);
	}
	m_DetectionManager.ResetCounter();
	m_DetectionManager.SetStreamType(DetectionManager::StreamType::KINECT_STREAM);
}

void PeopleDetector::fileStreamActivate(bool checked)
{
	if (checked)
	{
		m_DetectionManager.StopThread();
		m_KinectController.StopStream();

		ui.actionConnect->setDisabled(true);
		ui.actionSource_folder->setDisabled(false);
		ui.actionOutput_folder->setDisabled(false);
		ui.playPshBtn->setDisabled(false);
		ui.recordPshBtn->setDisabled(false);
		ui.detectPshBtn->setDisabled(false);
		ui.openFilePshBtn->setDisabled(false);
		ui.outputPath->setDisabled(false);
		ui.actionConnect->setDisabled(true);
		ui.threshVal->setDisabled(false);
		// m_DetectionManager.ResetCounter(); done in openSourceFolder();
		m_DetectionManager.SetStreamType(DetectionManager::StreamType::FILE_STREAM);
		openSourceFolder();
	}
}

void PeopleDetector::openSourceFolder()
{
	QString directory = QFileDialog::getExistingDirectory(new QWidget(), tr("Select source directory"), QString(m_FileManager.GetSourceDirectoryName().c_str()), QFileDialog::DontResolveSymlinks);

	if (!directory.isEmpty())
	{
		ui.statusBar->showMessage("Source Directory: \"" + directory + "\"");
		m_FileManager.SetSourceDirectory(directory.toStdString());
		m_DetectionManager.ResetCounter();
	}
}

void PeopleDetector::openOutputFolder()
{
	on_openFilePshBtn_clicked();
}

void PeopleDetector::actionThreshold()
{
	ui.statusBar->showMessage("Threshold segmentation method selected");
	ui.threshLbl->setText("Threshold val.");
	ui.threshVal->setDisabled(false);
	m_DetectionManager.SetSegmentationMethod(DetectionManager::SegmentationMethod::THRESHOLD,ui.threshVal->value());
}

void PeopleDetector::actionThresholdOTSU()
{
	m_DetectionManager.SetSegmentationMethod(DetectionManager::SegmentationMethod::THRESHOLD_OTSU);
	ui.threshVal->setDisabled(false);
	ui.statusBar->showMessage("Threshold OTSU segmentation method selected");
	ui.threshLbl->setText("Min. human height (mm)");
}

void PeopleDetector::actionAdaptiveThreshold()
{
	m_DetectionManager.SetSegmentationMethod(DetectionManager::SegmentationMethod::THRESHOLD_ADAPTIVE);
	ui.threshVal->setDisabled(true);
	ui.statusBar->showMessage("Threshold Adaptive (gaussian) segmentation method selected");
}

void PeopleDetector::actionReconstruction()
{
	m_DetectionManager.SetSegmentationMethod(DetectionManager::SegmentationMethod::RECONSTRUCTION);
	ui.threshVal->setDisabled(false);
	ui.statusBar->showMessage("Morphologic Reconstrucion segmentation method selected");
	ui.threshLbl->setText("Min. human height (mm)");
}

void PeopleDetector::actionLucas_Kanade()
{
	m_DetectionManager.m_Tracker.SetTrackingMethod(Tracker::TrackingMethods::OF_PYR_LK);
	ui.statusBar->showMessage("Lucas-Kanade method for optical flow tracking selected");
}

void PeopleDetector::actionSimpleBlob()
{
	m_DetectionManager.m_Tracker.SetTrackingMethod(Tracker::TrackingMethods::FT_SIMPLEBLOB);
	ui.statusBar->showMessage("SimpleBlobDetector method for feature tracking selected");
}

void PeopleDetector::actionFarneback()
{
	m_DetectionManager.m_Tracker.SetTrackingMethod(Tracker::TrackingMethods::OF_FARNEBACK);
	ui.statusBar->showMessage("Farneback method for optical flow tracking selected");
}

void PeopleDetector::actionResetCounters()
{
	m_DetectionManager.m_Tracker.Reset();
	m_DetectionManager.m_PeopleCounter.ResetCounters();
	unsigned long visited,monitored,left;
	m_DetectionManager.m_PeopleCounter.GetAllCounts(visited,monitored,left);
	ui.statusBar->showMessage("People counters reset - visited: " + QString::number(visited) + " monitored: " + QString::number(monitored)  + " left: " + QString::number(left));
}

void PeopleDetector::actionPeopleCounterSettings()
{
	m_CounterSettingsDialog.SetLinesOrientation(m_DetectionManager.m_PeopleCounter.GetOrientation());
	m_CounterSettingsDialog.SetLinesPosition(m_DetectionManager.m_PeopleCounter.GetDoorLine(),m_DetectionManager.m_PeopleCounter.GetAreaLine());
	m_CounterSettingsDialog.exec();
}

void PeopleDetector::actionSetDataPath()
{
	QString directory = QFileDialog::getExistingDirectory(new QWidget(), tr("Select logging directory"), QString(m_FileManager.GetPathToLoggingDirectory().c_str()), QFileDialog::DontResolveSymlinks);

	if (!directory.isEmpty())
	{
		ui.statusBar->showMessage("New logs will be saved to directory: \"" + directory + "\"");
		m_FileManager.SetPathToLogDirectory(directory.toStdString());
	}
}

void PeopleDetector::showImages(Mat *colorMat, Mat *depthMat, long counter)
{
	Mat color, depth;
	long ct;
	guiMutex.lock();
	colorMat->copyTo(color);
	depthMat->copyTo(depth);
	ct = counter;
	guiMutex.unlock();

	ui.colorImage->setPixmap(QPixmap::fromImage(MatToQImage(color)));
	ui.depthImage->setPixmap(QPixmap::fromImage(MatToQImage(depth)));
	ui.counterLbl->setText(QString("Frame: ") + QString::number(ct));
}

void PeopleDetector::fileStreamStatus(QString text)
{
	ui.statusBar->showMessage(QString(text));
}

void PeopleDetector::setKinectPitch(int angle)
{
	m_KinectController.ChangeAngle(angle);
}

QImage PeopleDetector::MatToQImage(Mat src)
{
	Mat tmp;
	switch (src.type())
	{
		// 8-bit, 4 channel
	case CV_8UC4:
		{
			QImage dest( src.data, src.cols, src.rows, src.step, QImage::Format_RGB32 );

			return dest.copy();
		}

		// 8-bit, 3 channel
	case CV_8UC3:
		{
			QImage dest( src.data, src.cols, src.rows, src.step, QImage::Format_RGB888 );

			return dest.copy();
		}

		// 16-bit, 1 channel
	case CV_16U:
		{
			QImage dest( src.data, src.cols, src.rows, src.step, QImage::Format_RGB16);

			return dest.copy();
		}

		// 8-bit, 1 channel
	case CV_8U :
		{
			Mat dst;
			cvtColor(src, dst, CV_GRAY2RGB);
			QImage dest( dst.data, dst.cols, dst.rows, dst.step, QImage::Format_RGB888);
			return dest.copy();
		}

	default:

		break;
	}
	return QImage(src.cols, src.rows, QImage::Format_Mono);
}