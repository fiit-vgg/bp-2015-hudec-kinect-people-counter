#include <opencv2\opencv.hpp>
#include <opencv2\core\core.hpp>
#include <opencv2\highgui\highgui.hpp>
#include <opencv2\objdetect\objdetect.hpp>
#include "Person.h"

#pragma once

using namespace cv;
class Tracker
{
public:

	enum TrackingMethods
	{
		OF_PYR_LK,
		OF_FARNEBACK,
		FT_SIMPLEBLOB
	};

	Tracker(void);
	~Tracker(void);
	void SetTrackingMethod(int method);
	// Initializes tracker with first frame of stream	
	void Initialize(Mat firstFrame, InputArray mask, InputArray heigthImage);
	Mat operator() (Mat nextFrame, InputArray mask, InputArray heigthImage);
	void Reset();

	bool IsInitialized();

	vector<Person> m_TrackedPeople;

private:
	
	// suggested value 10000
	int m_MaxThreshDistance;
	// suggested value 70
	int m_MaxLostTrackDistance;

	Mat m_GrayMask;

	Mat m_ImagePrevious;

	// Pyramid LK
	std::vector<cv::Point2f> m_FeaturesPrevious;	
	int m_CornersMax;
	double m_QualityLevel;
	double m_MinDistance;

	// Farneback
	double m_PyrScale;
	int m_Levels;
	int m_WinSize;
	int m_Iterations;
	int m_PolyN;
	double m_PolySigma;
	int m_Flags;

	// SimpleBlob
	SimpleBlobDetector::Params m_BlobParams;
	SimpleBlobDetector *m_BlobDetector;
	vector<KeyPoint> m_PreviousKeyPoints;
	Mat m_ImageBlobMaskPrevious;
	float m_MaxFistHeadSize;
	float m_MaxFistHeadHeigth;

	int m_Method;

	bool m_Initialized;

	void drawOptFlowMap(const Mat& flow, Mat& cflowmap, int step, double scale, const Scalar& color);
	Mat grayMaskNextFrame(InputArray nextFrame, InputArray mask);
	
	// swaps values in vectors according to their mutual distance - return vector contains square of these distances = if contains 409600 value is invalid
	vector<int> matchTwoKeyPointVectors(vector<KeyPoint> &one, vector<KeyPoint> &two)
	{
		int index = 0;
		int k = 0;
		int length = one.size() > two.size() ? one.size() : two.size();
		vector<int> distances(length,409600);
		int minDistance;
		for (int i = 0; i < one.size(); i++)
		{
			int index = i;
			minDistance = 409600; //640 * 640
			for (int j = 0; j < two.size(); j++)
			{
				int distance = pow((one[i].pt.x - two[j].pt.x),2) + pow((one[i].pt.y - two[j].pt.y),2);
				if (distance <= minDistance && distance <= distances[j])
				{
					minDistance = distance;
					index = j;
				}
			}
			if (one.size() <= two.size())
			{
				distances[i] = minDistance;
				std::swap(two[index],two[i]); // if one is smaller than two
			}
			else
			{
				distances[index] = minDistance;
				std::swap(one[index],one[i]); // if one is larger than two
				if (index != i)
				{
					i--;
				}
			}
		}
		return distances;
	}

	int m_RadicalCounter;
	Mat m_TrackerMask;
	float m_Alpha;
	RNG rng;

	cv::Scalar RandomColor()
	{
		unsigned int icolor = (unsigned) rng;
		return Scalar( icolor & 255, (icolor >> 8) & 255, (icolor >> 16) & 255 );
	}
};

