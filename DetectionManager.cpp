#include "stdafx.h"
#include "DetectionManager.h"

#define RECONSTRUCTION_SUBTRACTION 90
DetectionManager::DetectionManager(void) : 
	m_pDataStreamManager(NULL),
	m_pStreamMutex(NULL),
	m_bIsPlaying(false),
	m_bIsDetecting(false),
	m_iProcessStream(KINECT_STREAM),
	m_iSegmentationMethod(RECONSTRUCTION),
	m_noiseThreshold(30),
	m_FloorDepth(0),
	m_MinimalHumanHeight(0)
{
	m_bgHOWM.initialize(m_noiseThreshold);
	m_iThreshold = 0;
	m_counter = 0;
#ifdef _DEBUG
	namedWindow("Foreground");
#endif
}


DetectionManager::~DetectionManager(void)
{
	m_bIsPlaying = false;
	m_pDataStreamManager = NULL;
	m_pStreamMutex = NULL;
}


void DetectionManager::SetStreamMutex(HANDLE pStreamMutex)
{
	m_pStreamMutex = &pStreamMutex;
}


void DetectionManager::SetGuiMutex(QMutex *pGuiMutex)
{
	m_guiMutex = pGuiMutex;
}


void DetectionManager::SetDataStreamManager(KinectDataStreamManager * pDataStreamManager)
{
	m_pDataStreamManager = pDataStreamManager;
	m_pStreamMutex = m_pDataStreamManager->GetImageStreamMutex();
}


void DetectionManager::SetFileStreamManager(FileStreamManager * pFileStreamManager)
{
	m_pFileStreamManager = pFileStreamManager;
}


void DetectionManager::SetStreamType(int type)
{
	if (type == KINECT_STREAM || type == FILE_STREAM)
	{
		m_iProcessStream = type;
	}
}


void DetectionManager::SetSegmentationMethod(int type, int threshold)
{
	m_iSegmentationMethod = type;
	m_iThreshold = threshold;
}


void DetectionManager::SetThresholdValue(int threshold)
{
	m_iThreshold = threshold;
}


void DetectionManager::SetMinimalHumanHeightToTrack(USHORT threshold)
{
	m_MinimalHumanHeight = threshold;
}


void DetectionManager::run(void)
{
	m_bIsPlaying = true;

	while (m_bIsPlaying) {
		m_guiMutex->lock();

		switch(m_iProcessStream)
		{
		case KINECT_STREAM:
			ProcessKinectStream();
			break;
		case FILE_STREAM:
			if (ProcessFileStream() == false)
			{
				m_guiMutex->unlock();
				continue;
			}
			break;
		default: 
			m_guiMutex->unlock();
			continue;
		}

		if (m_bIsDetecting)
		{
			SegmentateImage();
//			std::clock_t start = std::clock();
			TrackImage();
//			double duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;
			PeopleCounting();
		}

		if (m_pFileStreamManager->IsRecording())
		{
			m_pFileStreamManager->saveColorImageFromMat(m_colorMat);
			m_pFileStreamManager->saveDepthImageFromMat(m_depthMat);
		}

		static const QMetaMethod changedsingnal = QMetaMethod::fromSignal(&DetectionManager::imagesLoaded);
		if (isSignalConnected(changedsingnal))
		{
			emit imagesLoaded(&m_colorMat, &m_depthMat, m_counter);
		}
		m_guiMutex->unlock();
		if (m_pFileStreamManager->IsRecording())
		{
			Sleep(5);
		}
		else if (m_bIsDetecting)
		{
			Sleep(10);
		}
		else
		{
			Sleep(33);
		}

	}
}


void DetectionManager::StopThread()
{
	m_bIsPlaying = false;
}


void DetectionManager::ResetCounter()
{
	m_counter = 0;
	m_Tracker.Reset();
	m_PeopleCounter.ResetCounters();
}


void DetectionManager::ResetBackgroundSubtractor()
{
	m_bgHOWM.initialize(m_noiseThreshold);
	m_FloorDepth = 0;
}


void DetectionManager::ProcessKinectStream()
{
	WaitForSingleObject(*m_pStreamMutex, INFINITE);

	if (!m_colorMat.empty())
	{
		m_colorMat.release();
	}
	if (!m_depthMat.empty())
	{
		m_depthMat.release();
	}

	m_colorMat = m_pDataStreamManager->GetColorImage();
	m_depthMat = m_pDataStreamManager->GetDepthImage();

	ReleaseMutex(*m_pStreamMutex);
	m_counter++;
}


bool DetectionManager::ProcessFileStream()
{
	if (m_pFileStreamManager->GetNextColorImage(&m_colorMat) && m_pFileStreamManager->GetNextDepthImage(&m_depthMat))
	{
		m_counter++;
		return true;
	}
	else
	{
		m_bIsPlaying = false;
		static const QMetaMethod changedsingnal = QMetaMethod::fromSignal(&DetectionManager::statusChanged);
		if (isSignalConnected(changedsingnal))
		{
			emit statusChanged("File stream ended");
		}
		return false;
	}
}

void DetectionManager::DetectionStatus(bool status)
{
	m_bIsDetecting = status;
}

void DetectionManager::SegmentateImage()
{
	Mat tmp;
	
	m_FloorDepth = depth16ToHeigth16(m_depthMat, tmp, m_FloorDepth);
//  copy the heigth mat into backup for future use in fist/head detection	
	tmp.copyTo(m_depthMatHeigthBackup);

	m_bgHOWM(tmp,m_fgMask,0.0001);
#ifdef _DEBUG
	imshow("Foreground",m_fgMask);
#endif
	Mat segmentatedImage;
	tmp.copyTo(segmentatedImage,m_fgMask);
//	imshow("Foreground",segmentatedImage);
	switch(m_iSegmentationMethod)
	{
	case THRESHOLD :
		{
			segmentatedImage = cvtHeight16U_8U(segmentatedImage);
			threshold(segmentatedImage,segmentatedImage,m_iThreshold,255,CV_THRESH_BINARY);
			segmentatedImage.copyTo(m_depthMat);
			break;
		}
	case THRESHOLD_OTSU :
		{
			if (m_MinimalHumanHeight)
			{
				threshold16ToZero(segmentatedImage,segmentatedImage,m_MinimalHumanHeight);
			}
			segmentatedImage = cvtHeight16U_8U(segmentatedImage);
			threshold(segmentatedImage,segmentatedImage,m_iThreshold,255,CV_THRESH_OTSU);
			segmentatedImage.copyTo(m_depthMat);
			break;
		}
	case THRESHOLD_ADAPTIVE :
		{
			if (m_MinimalHumanHeight)
			{
				threshold16ToZero(segmentatedImage,segmentatedImage,m_MinimalHumanHeight);
			}
			segmentatedImage = cvtHeight16U_8U(segmentatedImage);
			adaptiveThreshold(segmentatedImage,m_depthMat,255,ADAPTIVE_THRESH_GAUSSIAN_C,CV_THRESH_BINARY,5,1.5);
			break;
		}
	case RECONSTRUCTION :
		{
			if (m_MinimalHumanHeight)
			{
				threshold16ToZero(segmentatedImage,segmentatedImage,m_MinimalHumanHeight);
			}
			IplImage *mask = new IplImage(segmentatedImage);
			Mat tmp = subtractConst(segmentatedImage, RECONSTRUCTION_SUBTRACTION);
			IplImage *marker = new IplImage(tmp);
			
			ImReconstruct(marker,mask);
			tmp = Mat(marker, true);
			subtract(segmentatedImage, tmp, segmentatedImage);
			segmentatedImage = subtractConst(segmentatedImage,20);

			segmentatedImage.convertTo(m_depthMat,CV_8U);
			threshold(m_depthMat,m_depthMat,0,255,CV_THRESH_BINARY);
			
			Mat kernel = getStructuringElement(MORPH_ELLIPSE,Size(30,30));
			morphologyEx(m_depthMat,m_depthMat,MORPH_OPEN,kernel);
			kernel = getStructuringElement(MORPH_ELLIPSE,Size(10,10));
			erode(m_depthMat,m_depthMat,kernel);
			break;
		}
	}
}


// optical flow - PyrLK, HS, Farneback, || erode
void DetectionManager::TrackImage()
{
	if (m_Tracker.IsInitialized())
	{
		m_colorMat = m_Tracker(m_colorMat,m_depthMat,m_depthMatHeigthBackup);
	}
	else
	{
		m_Tracker.Initialize(m_colorMat,m_depthMat,m_depthMatHeigthBackup);
	}
}


void DetectionManager::PeopleCounting()
{
	m_PeopleCounter(m_Tracker.m_TrackedPeople);
	unsigned long visited, leavers, inRoom;
	m_PeopleCounter.GetAllCounts(visited,leavers,inRoom);
	Point2f doorEnd, areaEnd;
	m_PeopleCounter.GetLinesEndpoints(doorEnd,areaEnd);
	line(m_colorMat,m_PeopleCounter.GetAreaLine(),areaEnd,Scalar(255,255,255),1,4);
	line(m_colorMat,m_PeopleCounter.GetDoorLine(),doorEnd,Scalar(255,255,255),1,4);

	// do some magic //
	std::string text;
	text = "Visited: " + std::to_string(visited);
	putText(m_colorMat,text,Point(10,20),FONT_HERSHEY_SIMPLEX,0.7,Scalar(0,255,0));
	text = "Left: " + std::to_string(leavers);
	putText(m_colorMat,text,Point(10,40),FONT_HERSHEY_SIMPLEX,0.7,Scalar(0,255,0));
	text = "In the room: " + std::to_string(inRoom);
	putText(m_colorMat,text,Point(10,60),FONT_HERSHEY_SIMPLEX,0.7,Scalar(0,255,0));
}


Mat DetectionManager::cvtDepth16U_8U(Mat depthImage)
{
	if (depthImage.type() != CV_16U) return depthImage;

	Mat tmp;
	tmp.create(depthImage.rows,depthImage.cols,CV_8U);
	for (UINT y = 0; y < depthImage.rows; y++)
	{
		// Get row pointer for depth Mat
		USHORT* pImageRow = depthImage.ptr<USHORT>(y);
		BYTE* pTmpRow = tmp.ptr<BYTE>(y);

		for (UINT x = 0; x < depthImage.cols; x++)
		{
			if (pImageRow[x] != 0)
			{
				USHORT tmpu = (pImageRow[x] / 10.0 + 0.5);
				tmpu = tmpu - 80 < 0 ? 0 : tmpu - 80;
				// invert depth to "height"
				tmpu = 400 - tmpu;
				tmpu = TRUNCATE(tmpu);
				pTmpRow[x] = static_cast<BYTE>(tmpu);
			}
		}
	}

	return tmp.clone();
}

Mat DetectionManager::cvtHeight16U_8U(Mat depthImage)
{
	Mat tmp;
	tmp.create(depthImage.rows,depthImage.cols,CV_8U);

	for (UINT y = 0; y < depthImage.rows; y++)
	{
		// Get row pointer for depth Mat
		USHORT* pImageRow = depthImage.ptr<USHORT>(y);
		BYTE* pTmpRow = tmp.ptr<BYTE>(y);

		for (UINT x = 0; x < depthImage.cols; x++)
		{
			if (pImageRow[x] != 0)
			{
				pTmpRow[x] = pImageRow[x] / 10 - 0.5;
			}
			else
			{
				pTmpRow[x] = 0;
			}
		}
	}
	return tmp.clone();
}

Mat DetectionManager::subtractConst(Mat image, int value)
{
	Mat tmp;
	tmp.create(image.rows,image.cols,image.type());

	for (UINT y = 0; y < image.rows; y++)
	{
		// Get row pointer for depth Mat
		if (image.type() == CV_8U)
		{
			BYTE* pImageRow = image.ptr<BYTE>(y);
			BYTE* pTmpRow = tmp.ptr<BYTE>(y);

			for (UINT x = 0; x < image.cols; x++)
			{
				pTmpRow[x] = pImageRow[x] - value < 0 ? 0 : pImageRow[x] - value;
			}
		}
		else if (image.type() == CV_16U)
		{
			USHORT* pImageRow = image.ptr<USHORT>(y);
			USHORT* pTmpRow = tmp.ptr<USHORT>(y);

			for (UINT x = 0; x < image.cols; x++)
			{
				pTmpRow[x] = pImageRow[x] - value < 0 ? 0 : pImageRow[x] - value;
			}
		}
	}
	return tmp.clone();
}

int DetectionManager::depth16ToHeigth16(Mat depthImage, Mat &dst, int floorDepth)
{
	if (depthImage.type() != CV_16U) 
	{
		depthImage.copyTo(dst);
		return 0;
	}

	Mat tmp;
	USHORT maxDepth = floorDepth;
	UINT w = 0, h = 0;
	tmp.create(depthImage.rows,depthImage.cols,CV_16U);

	if (maxDepth == 0)
	{
		for (UINT y = 0; y < depthImage.rows; y++)
		{
			// Get row pointer for depth Mat
			USHORT* pImageRow = depthImage.ptr<USHORT>(y);

			for (UINT x = 0; x < depthImage.cols; x++)
			{
				// find floor depth
				if (pImageRow[x] > maxDepth)
				{
					maxDepth = pImageRow[x];
					w = x;
					h = y;
				}
			}
		}
	}

	for (UINT y = 0; y < depthImage.rows; y++)
	{
		// Get row pointer for depth Mat
		USHORT* pImageRow = depthImage.ptr<USHORT>(y);
		USHORT* pTmpRow = tmp.ptr<USHORT>(y);

		for (UINT x = 0; x < depthImage.cols; x++)
		{
			// convert depth to height
			if (pImageRow[x] != 0)
			{
				pTmpRow[x] = maxDepth - pImageRow[x] < 0 ? 0 : maxDepth - pImageRow[x];
			}
			else
			{
				pTmpRow[x] = 0;
			}

		}
	}

	tmp.copyTo(dst);
	return maxDepth;
}
