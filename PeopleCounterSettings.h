#pragma once
#include "qdialog.h"
#include "ui_PeopleCounterSettings.h"
#include "PeopleCounter.h"
#include <opencv2\opencv.hpp>

using namespace cv;
class PeopleCounterSettings :
	public QDialog
{

	Q_OBJECT

public:
	PeopleCounterSettings(QWidget *parent = 0);
	~PeopleCounterSettings(void);

	void SetLinesOrientation(int orientation);
	void SetLinesPosition(Point2f door, Point2f area);

public slots:
	void changeDoorMin(int min);
	void changeAreaMax(int max);
	void changeDoorLinePosition(int value);
	void changeAreaLinePosition(int value);
	void orientationBoxClicked(bool value);
	
protected:
	virtual void accept();
	virtual void reject();

signals:
	void doorLinePositionChanged(Point2f position);
	void areaLinePositionChanged(Point2f position);
	// true == vertically; false == horizontally
	void orientationChanged(bool orientation);
	void initialPeopleInRoomSet(int people);

private:
	Ui::CounterSettingsDialog ui;

	int m_Orientation;
	Point2f m_Door;
	Point2f m_Area;
};