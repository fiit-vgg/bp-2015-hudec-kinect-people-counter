#pragma once

#include <opencv2\opencv.hpp>

class FileStreamManager : public QObject
{
	Q_OBJECT

public:
	FileStreamManager(void);
	~FileStreamManager(void);

	// Choose directory which contains at least one subdirectory - Color/Depth
	void SetSourceDirectory(std::string directoryName);
	// If does not exist - creates new folder
	void SetOutputDirectory(std::string directoryName);

	std::string GetSourceDirectoryName();
	std::string GetOutputFolderName();

	void StartRecording();
	void StopRecording();
	bool IsRecording();

	bool GetNextColorImage(cv::Mat *image);
	bool GetNextDepthImage(cv::Mat *image);

	HRESULT SetPathToLogDirectory(const std::string directoryName);
	HRESULT OpenLogFile();
	BOOL CloseLogFile();
	std::string GetPathToLoggingDirectory();

private:
	bool InitializeOutputDirectory();

public slots:
	void saveColorImageFromMat(cv::Mat image);
	void saveDepthImageFromMat(cv::Mat image);
	void logRoomMovement(std::string direction, int inRoom, SYSTEMTIME timestamp);

private:
	std::string m_SourceDirectoryName;
	std::string m_OutputFolderName;

	std::string m_ColorPrefix;
	std::string m_ColorSuffix;
	std::string m_DepthPrefix;
	std::string m_DepthSuffix;
	long m_ColorSourceOrder;
	long m_DepthSourceOrder;

	long m_ColorOutputOrder;
	long m_DephtOutputOrder;

	bool m_bIsRecording;

	// Logging to .csv
	std::string m_PathToLoggingDirectory;
	std::string m_LogFileName;
	HANDLE m_LogFileHandle;

	std::wstring StringToWString(const std::string& s)
	{
		int len;
		int slength = (int)s.length() + 1;
		len = MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, 0, 0); 
		wchar_t* buf = new wchar_t[len];
		MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, buf, len);
		std::wstring r(buf);
		delete[] buf;
		return r;
	}

};
