#pragma once
#include "qdialog.h"
#include "ui_KinectPitchChange.h"

class KinectPitch :
	public QDialog
{
	Q_OBJECT

public:
	KinectPitch(QWidget *parent = 0);

	~KinectPitch(void);

	void SetActualAngle(long angle);

protected:
	virtual void accept();
	virtual void reject();

signals:
	void valueChanged(int angle);

public slots:
	void changeValue(int angle);

private:
	Ui::PitchDialog ui;
	int m_ActualAngle;

};

