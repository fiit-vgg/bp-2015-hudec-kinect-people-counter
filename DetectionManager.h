#ifndef DETECTIONMANAGER_H
#define DETECTIONMANAGER_H

#pragma once

#pragma warning(push)
#pragma warning(disable : 6294 6031)

#include <opencv2\core\core.hpp>
#include <opencv2\opencv.hpp>
#include <opencv2\highgui\highgui.hpp>
#include <opencv2\video\background_segm.hpp>
#include <qthread.h>
#include <ctime>
#include "KinectDataStreamManager.h"
#include "FileStreamManager.h"
#include "BackgroundSubtractorHOWM.h"
#include "ImageReconstruct.h"
#include "Tracker.h"
#include "PeopleCounter.h"

#pragma warning(pop)

#define TRUNCATE(x) x < 256 ? x >= 0 ? x : 0 : 255

using namespace cv;
class DetectionManager : public QThread
{
	Q_OBJECT

public:
	DetectionManager(void);
	~DetectionManager(void);

	enum StreamType 
	{
		KINECT_STREAM,
		FILE_STREAM
	};

	enum SegmentationMethod
	{
		THRESHOLD,
		THRESHOLD_OTSU,
		THRESHOLD_ADAPTIVE,
		RECONSTRUCTION
	};
	
	Tracker m_Tracker;
	PeopleCounter m_PeopleCounter;

private:
	KinectDataStreamManager *m_pDataStreamManager;
	FileStreamManager *m_pFileStreamManager;

	Mat m_colorMat;
	Mat m_depthMat;
	Mat m_depthMatHeigthBackup;

	long m_FloorDepth;
	long m_counter;
	HANDLE *m_pStreamMutex;
	QMutex *m_guiMutex;
	bool m_bIsPlaying;
	bool m_bIsDetecting;
	int m_iProcessStream;

	int m_iSegmentationMethod;
	int m_iThreshold;

	BackgroundSubtractorHOWM m_bgHOWM;
	Mat m_fgMask;
	int m_noiseThreshold;
	// in case of minimal heigth of human(kids) that should be tracked
	USHORT m_MinimalHumanHeight;

public:
	void SetStreamMutex(HANDLE pStreamMutex);
	void SetGuiMutex(QMutex *pGuiMutex);
	void StopThread();
	// Sets KinectDataStreamManager
	// - loads its mutex for running thread
	// - initializes matrices
	void SetDataStreamManager(KinectDataStreamManager * pDataStreamManager);
	void SetFileStreamManager(FileStreamManager * pFileStreamManager);
	void SetStreamType(int type = KINECT_STREAM);
	// Set segmentation method from enum, set threshold value when using threshold method
	void SetSegmentationMethod(int type, int threshold = 0);
	// Explicit way to set the threshold value
	void SetThresholdValue(int threshold);
	// Sets the minimal human heigth to be considered as trackable human
	void SetMinimalHumanHeightToTrack(USHORT threshold);
	// Set true if you want to run detection, false to stop
	void DetectionStatus(bool status);
	// Resets the counter of processed frames
	void ResetCounter();
	void ResetBackgroundSubtractor();
	

	void threshold16ToZero(InputArray src, OutputArray dst, USHORT threshold)
	{
		Mat srcMat = src.getMat();

		if (srcMat.empty()) return;

		dst.create(src.size(),src.type());
		Mat dstMat = dst.getMat();

		for (UINT y = 0; y < srcMat.rows; y++)
		{
			// Get row pointer for Mat
			USHORT* pSrcRow = srcMat.ptr<USHORT>(y);
			USHORT* pDstRow = dstMat.ptr<USHORT>(y);

			for (UINT x = 0; x < srcMat.cols; x++)
			{
				if (pSrcRow[x] < threshold)
				{
					pDstRow[x] = 0;
				}
				else
				{
					pDstRow[x] = pSrcRow[x];
				}
			}
		}
		dstMat.copyTo(dst);
	}

signals:
	void imagesLoaded(Mat* colorMat, Mat* depthMat, long counter);
	void statusChanged(QString text);

protected:
	void run(void);

private:
	void ProcessKinectStream();
	bool ProcessFileStream();

	void SegmentateImage();
	void TrackImage();
	void PeopleCounting();

	Mat cvtDepth16U_8U(Mat);
	Mat cvtHeight16U_8U(Mat);
	Mat subtractConst(Mat, int);
	int depth16ToHeigth16(Mat image, Mat &dst, int floorDepth = 0);
};

#endif 

