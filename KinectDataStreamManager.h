#pragma once

#pragma warning(push)
#pragma warning(disable : 6294 6031)

#include <NuiApi.h>
#include <stdlib.h>
#include <algorithm>
#include <iterator>

#include <opencv2\core\core.hpp>
#include <opencv2\opencv.hpp>

#pragma warning(pop)

using namespace cv;

class KinectDataStreamManager
{
public:
	KinectDataStreamManager(void);
	~KinectDataStreamManager(void);
	
	// Constants
	// Mat type for each usage
	static const int COLOR_TYPE = CV_8UC4;
	static const int DEPTH_TYPE = CV_16U;
	static const NUI_IMAGE_RESOLUTION COLOR_RESOLUTION = NUI_IMAGE_RESOLUTION_640x480;
    static const NUI_IMAGE_RESOLUTION DEPTH_RESOLUTION = NUI_IMAGE_RESOLUTION_640x480;

    // Initializes the Kinect with the given sensor. The depth stream and color stream,
    // if they are opened, will be set to the default resolutions defined in COLOR_DEFAULT_RESOLUTION
    // and DEPTH_DEFAULT_RESOLUTION.
    HRESULT Initialize(INuiSensor* pSensor);

	// Initializes OpenCV matrices
	void InitializeMatrices();

	// Uninitializes the Kinect
	void UnInitialize(void);

	// Returns whether or not the Kinect has been initialized
	// - true if the Kinect is initialized, false otherwise
	bool IsInitialized(void) const;

	// Updates the internal color image
	// - parameter defines number of milliseconds to wait
	// - returns S_OK if successful, an error code otherwise
	HRESULT UpdateColorFrame(DWORD waitMillis = 0);

	// Updates the internal depth image
	// - parameter defines number of milliseconds to wait
	// - returns S_OK if successful, an error code otherwise
	HRESULT UpdateDepthFrame(DWORD waitMillis = 0);

	// Gets the color stream resolution
	// - returns S_OK if successful, an error code otherwise
	HRESULT GetColorFrameSize(DWORD* width, DWORD* height) const;

	// Gets the depth stream resolution
	// - returns S_OK if successful, an error code otherwise
	HRESULT GetDepthFrameSize(DWORD* width, DWORD* height) const;

	// Converts color data stream into OpenCV Mat format
	HRESULT GetColorData(Mat* pImage);
	// Converts depth data stream into OpenCV Mat format
	HRESULT GetDepthData(Mat* pImage);

	// Synchronized method to obtain Color Image from Kinect stream
	Mat GetColorImage();
	// Synchronized method to obtain Depth Map from Kinect stream
	Mat GetDepthImage();

	HRESULT StartStream();
	void StopStream();
	// Stream thread starter function
	DWORD WINAPI StreamThread();
	static DWORD WINAPI StartStreamThread(LPVOID lpParam);

	HANDLE * GetImageStreamMutex();

private:
	// Image stream data
	BYTE* m_pColorBuffer;
	INT m_colorBufferSize;
	INT m_colorBufferPitch;
	BYTE* m_pDepthBuffer;
	INT m_depthBufferSize;
	INT m_depthBufferPitch;
	LONG *m_pColorCoordinates;
	bool m_bCompute;
	DWORD m_ImageWidth;
	DWORD m_ImageHeight;

	// Variables:
	// Image stream handles
	HANDLE m_hColorStreamHandle;
	HANDLE m_hDepthStreamHandle;

	// Frame event handles
	// These are handles to events created using the CreateEvent Win32 API
	HANDLE m_hNextColorFrameEvent;
	HANDLE m_hNextDepthFrameEvent;

	// Kinect initialization flags
	DWORD m_InitFlags;

	// Pointer to Kinect sensor
	INuiSensor* m_pSensor;

	// Stream mutex
	HANDLE m_hImageStreamMutex;
	// Image handling synchronization mutex
	HANDLE m_hSynchronizationGetterMutex;
	// Stream thread handle
	HANDLE m_hImageStreamThread;
	
	bool m_bContinueStream;

	// OpenCV matrices
	Mat m_ColorImage;
	Mat m_DepthImage;
};

