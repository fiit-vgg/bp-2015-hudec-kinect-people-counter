#include "KinectDataStreamManager.h"
#include "KinectTiltAngleManager.h"

#pragma once

class KinectController
{
public:
	KinectController(void);
	~KinectController(void);
	
	// Returns whether or not the Kinect has been initialized
	// - true if the Kinect is initialized, false otherwise
	bool IsInitialized(void) const;
		
	// Initializes the first available Kinect found
	HRESULT InitializeFirstConnected();

	// Get the value of Kinect Pitch angle
	HRESULT GetAngle(long *degree);
	// Changes the value of Kinect Pitch angle, if no value post, resets angle to 0
	HRESULT ChangeAngle(long degree = 0);

	// Starts thread loading Kinect stream
	// - thread is defined in KinectDataStreamManager
	HRESULT OpenStream();

	void StopStream();

	// Returns pointer to its own KinectDataStreamManager
	KinectDataStreamManager* GetKinectDataStreamManager();
private:
	KinectDataStreamManager m_dataStreamManager;
	INuiSensor *m_pSensor;
	KinectTiltAngleManager m_tiltAngleManager;

};
