#include "stdafx.h"
#include "PeopleCounterSettings.h"


PeopleCounterSettings::PeopleCounterSettings(QWidget *parent)
{
	ui.setupUi(this);
}


PeopleCounterSettings::~PeopleCounterSettings(void)
{
}


void PeopleCounterSettings::SetLinesOrientation(int orientation)
{
	m_Orientation = orientation;
	bool b = orientation == PeopleCounter::Orientation::X_AXIS;
	ui.orientationVertical->setChecked(b);
	if (b)
	{
		ui.doorSpinBox->setMaximum(639);
		ui.areaSpinBox->setMaximum(639);
	}
	else
	{
		ui.doorSpinBox->setMaximum(479);
		ui.areaSpinBox->setMaximum(479);
	}
}


void PeopleCounterSettings::SetLinesPosition(Point2f door, Point2f area)
{
	m_Door = door;
	m_Area = area;
	if (m_Orientation == PeopleCounter::Orientation::X_AXIS)
	{
		ui.doorSpinBox->setValue(door.x);
		ui.areaSpinBox->setValue(area.x);
		changeDoorMin(area.x);
		changeAreaMax(door.x);
	}
	else
	{
		ui.doorSpinBox->setValue(door.y);
		ui.areaSpinBox->setValue(area.y);
		changeDoorMin(area.y);
		changeAreaMax(door.y);
	}
}


void PeopleCounterSettings::changeDoorMin(int min)
{
	ui.doorSpinBox->setMinimum(min+1);
}


void PeopleCounterSettings::changeAreaMax(int max)
{
	ui.areaSpinBox->setMaximum(max-1);
}


void PeopleCounterSettings::accept()
{
	emit initialPeopleInRoomSet(ui.peopleInRoomSpinBox->value());
	this->hide();
}


void PeopleCounterSettings::reject()
{
	emit doorLinePositionChanged(m_Door);
	emit areaLinePositionChanged(m_Area);
	emit orientationChanged(m_Orientation == PeopleCounter::Orientation::X_AXIS);
	this->hide();
}


void PeopleCounterSettings::changeDoorLinePosition(int value)
{
	Point2f pos;
	if (ui.orientationVertical->isChecked())
	{
		pos = Point2f(value,0);
	}
	else
	{
		pos = Point2f(0,value);
	}
	emit doorLinePositionChanged(pos);
}


void PeopleCounterSettings::changeAreaLinePosition(int value)
{
	Point2f pos;
	if (ui.orientationVertical->isChecked())
	{
		pos = Point2f(value,0);
	}
	else
	{
		pos = Point2f(0,value);
	}
	emit areaLinePositionChanged(pos);
}


void PeopleCounterSettings::orientationBoxClicked(bool value)
{
	Point2f posD;
	Point2f posA;

	changeAreaMax(ui.doorSpinBox->value());
		
	if (value)
	{
		ui.doorSpinBox->setMaximum(639);
		ui.areaSpinBox->setMaximum(639);
		posD = Point2f(ui.doorSpinBox->value(),0);
		posA = Point2f(ui.areaSpinBox->value(),0);
	}
	else
	{
		ui.doorSpinBox->setMaximum(479);
		ui.areaSpinBox->setMaximum(479);
		posD = Point2f(0,ui.doorSpinBox->value());
		posA = Point2f(0,ui.areaSpinBox->value());
	}

	changeDoorMin(ui.areaSpinBox->value());

	emit areaLinePositionChanged(posA);
	emit doorLinePositionChanged(posD);
	emit orientationChanged(value);
}
