#include "stdafx.h"
#include "Person.h"

Person::Person(void) 
{
}


Person::Person(Point2f position, Scalar color, Person::Directions state)
{
	m_Direction = state;
	m_PreviousPosition = m_ActualPosition = position;
	m_Color = color;
}


Person::~Person(void)
{
}


Scalar Person::GetLineColor()
{
	return m_Color;
}


double Person::SetNextPosition(Point2f nextPosition)
{
	m_PreviousPosition = m_ActualPosition;
	m_ActualPosition = nextPosition;
	return sqrt(pow((m_ActualPosition.x - m_PreviousPosition.x),2)+pow((m_ActualPosition.y - m_PreviousPosition.y),2));
}


void Person::SetDirectionState(Person::Directions direction)
{
	m_Direction = direction;
}


int Person::GetDirectionState()
{
	return m_Direction;
}


Point2f Person::GetPreviousPosition()
{
	return m_PreviousPosition;
}


Point2f Person::GetActualPosition()
{
	return m_ActualPosition;
}
