#include "stdafx.h"
#include "KinectDataStreamManager.h"

KinectDataStreamManager::KinectDataStreamManager(void) :
            m_hColorStreamHandle(NULL),
            m_hDepthStreamHandle(NULL),
            m_hNextColorFrameEvent(NULL),
            m_hNextDepthFrameEvent(NULL),
            m_pSensor(NULL),
            m_pColorBuffer(NULL),
            m_colorBufferSize(0),
            m_colorBufferPitch(0),
            m_pDepthBuffer(NULL),
            m_depthBufferSize(0),
            m_depthBufferPitch(0),
			m_bContinueStream(false),
			m_hImageStreamThread(NULL),
			m_bCompute(false)
{
	m_InitFlags = 0;
	m_InitFlags |= NUI_INITIALIZE_FLAG_USES_COLOR | NUI_INITIALIZE_FLAG_USES_DEPTH;
	
	m_hImageStreamMutex = CreateMutex(NULL, FALSE, NULL);
	m_hSynchronizationGetterMutex = CreateMutex(NULL,FALSE,NULL);
	InitializeMatrices();
	GetColorFrameSize(&m_ImageWidth, &m_ImageHeight);
	m_pColorCoordinates = new LONG[m_ImageWidth*m_ImageHeight*2];
}


KinectDataStreamManager::~KinectDataStreamManager(void)
{
	UnInitialize();
	TerminateThread(m_hImageStreamThread,0);
	
	if (m_hImageStreamThread) 
	{
		CloseHandle(m_hImageStreamThread);
	}
}

// Initializes the Kinect with the given sensor. The depth stream and color stream,
// if they are opened, will be set to the default resolutions defined in COLOR_DEFAULT_RESOLUTION
// and DEPTH_DEFAULT_RESOLUTION.
// - parameter = sensor to initialise
// - returns S_OK if succesful, error code otherwise
HRESULT KinectDataStreamManager::Initialize(INuiSensor* pSensor)
{
	HRESULT hr;

	// If there is no sensor, initialize one
	if (!m_pSensor)
	{
		if (!pSensor)
		{
			return E_POINTER;
		}
		m_pSensor = pSensor;

		hr = m_pSensor->NuiInitialize(m_InitFlags);
		if (FAILED(hr))
		{
			return hr;
		}

	}

	// Create stream events
	m_hNextColorFrameEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_hNextDepthFrameEvent = CreateEvent(NULL, TRUE, FALSE, NULL);

	// Open image stream
	hr = m_pSensor->NuiImageStreamOpen(
		NUI_IMAGE_TYPE_COLOR,
		COLOR_RESOLUTION,
		0,
		2,
		m_hNextColorFrameEvent,
		&m_hColorStreamHandle);
	if (FAILED(hr))
	{
		return hr;
	}

	// Open depth stream
	hr = m_pSensor->NuiImageStreamOpen(
		NUI_IMAGE_TYPE_DEPTH,
		DEPTH_RESOLUTION,
		0,
		2,
		m_hNextDepthFrameEvent,
		&m_hDepthStreamHandle);
	if (FAILED(hr))
	{
		return hr;
	}

	return hr;
}

void KinectDataStreamManager::InitializeMatrices()
{
	DWORD width, height;
	GetColorFrameSize(&width, &height);
	Size size(width,height);
	m_ColorImage.create(size, COLOR_TYPE);

	GetDepthFrameSize(&width, &height);
	size.height = height;
	size.width = width;

	m_DepthImage.create(size,DEPTH_TYPE);
}

// Uninitializes the Kinect
void KinectDataStreamManager::UnInitialize()
{
	// Close Kinect
	if (m_pSensor)
	{
		m_pSensor->NuiShutdown();
		m_pSensor = NULL;
	}

	// Close handles for created events
	if (m_hNextColorFrameEvent)
	{
		CloseHandle(m_hNextColorFrameEvent);
		m_hNextColorFrameEvent = NULL;
	}

	if (m_hNextDepthFrameEvent)
	{
		CloseHandle(m_hNextDepthFrameEvent);
		m_hNextDepthFrameEvent = NULL;
	}

}

// Returns true if the Kinect has been initialized, false otherwised
bool KinectDataStreamManager::IsInitialized() const
{
	return m_pSensor != NULL;
}

// Updates the internal color image
HRESULT KinectDataStreamManager::UpdateColorFrame(DWORD waitMillis /* = 0 */)
{
	// Fail if Kinect is not initialized
	if (!m_pSensor)
	{
		return E_NUI_DEVICE_NOT_READY;
	}

	// Get next image stream frame
	NUI_IMAGE_FRAME imageFrame;

	HRESULT hr = m_pSensor->NuiImageStreamGetNextFrame(
		m_hColorStreamHandle,
		waitMillis,
		&imageFrame);
	if (FAILED(hr))
	{
		return hr;
	}

	// Lock frame texture to allow for copy
	INuiFrameTexture* pTexture = imageFrame.pFrameTexture;
	NUI_LOCKED_RECT lockedRect;
	pTexture->LockRect(0, &lockedRect, NULL, 0);

	// Check if image is valid
	if (lockedRect.Pitch != 0)
	{
		// Copy image information into buffer so it doesn't get overwritten later
		BYTE* pBuffer = lockedRect.pBits;
		INT size =  lockedRect.size;
		INT pitch = lockedRect.Pitch;

		// Only reallocate memory if the buffer size has changed
		if (size != m_colorBufferSize)
		{
			delete [] m_pColorBuffer;
			m_pColorBuffer = new BYTE[size];
			m_colorBufferSize = size;
		}
		memcpy_s(m_pColorBuffer, size, pBuffer, size);


		m_colorBufferPitch = pitch;
	}

	// Unlock texture
	pTexture->UnlockRect(0);

	// Release image stream frame
	hr = m_pSensor->NuiImageStreamReleaseFrame(m_hColorStreamHandle, &imageFrame);

	return hr;
}

// Updates the internal depth image
HRESULT KinectDataStreamManager::UpdateDepthFrame(DWORD waitMillis /* = 0 */)
{
	// Fail if Kinect is not initialized
	if (!m_pSensor)
	{
		return E_NUI_DEVICE_NOT_READY;
	}
	
	// Get next image stream frame
	NUI_IMAGE_FRAME imageFrame;

	HRESULT hr = m_pSensor->NuiImageStreamGetNextFrame(
		m_hDepthStreamHandle,
		waitMillis,
		&imageFrame);
	if (FAILED(hr))
	{
		return hr;
	}

	// Lock frame texture to allow for copy
	INuiFrameTexture* pTexture = imageFrame.pFrameTexture;
	NUI_LOCKED_RECT lockedRect;
	pTexture->LockRect(0, &lockedRect, NULL, 0);

	// Check if image is valid
	if (lockedRect.Pitch != 0)
	{
		// Copy image information into buffer
		BYTE* pBuffer = lockedRect.pBits;
		INT size =  lockedRect.size;
		INT pitch = lockedRect.Pitch;

		// Reallocate memory if the buffer size has changed
		if (size != m_depthBufferSize)
		{
			delete [] m_pDepthBuffer;
			m_pDepthBuffer = new BYTE[size];
			m_depthBufferSize = size;
		}
		memcpy_s(m_pDepthBuffer, size, pBuffer, size);

		m_depthBufferPitch = pitch;
	}

	// Unlock texture
	pTexture->UnlockRect(0);

	// Release image stream frame
	hr = m_pSensor->NuiImageStreamReleaseFrame(m_hDepthStreamHandle, &imageFrame);

	return hr;
}

// Gets the color stream resolution
HRESULT KinectDataStreamManager::GetColorFrameSize(DWORD* width, DWORD* height) const
{
	if (!width || !height) 
	{
		return E_POINTER;
	}

	NuiImageResolutionToSize(COLOR_RESOLUTION, *width, *height);

	return S_OK;
}

/// Gets the depth stream resolution
HRESULT KinectDataStreamManager::GetDepthFrameSize(DWORD* width, DWORD* height) const
{
	if (!width || !height) 
	{
		return E_POINTER;
	}

	NuiImageResolutionToSize(DEPTH_RESOLUTION, *width, *height);

	return S_OK;
}

HRESULT KinectDataStreamManager::GetColorData(Mat* pImage)
{
	HRESULT hr;
	// Check if image is valid
    if (m_colorBufferPitch == 0)
    {
        return E_NUI_FRAME_NO_DATA;
    }

    DWORD colorHeight, colorWidth;
	hr = GetColorFrameSize(&colorWidth, &colorHeight);
	if (hr != S_OK)
	{
		return hr;
	}

    // Copy image information into Mat
    for (UINT y = 0; y < colorHeight; ++y)
    {
		for (UINT x = 0; x < colorWidth; ++x)
        {
			int depthIndex = x + y * m_ImageWidth;

			LONG translatedX = m_pColorCoordinates[depthIndex * 2];
			LONG translatedY = m_pColorCoordinates[depthIndex * 2 + 1];

			if (m_bCompute && translatedX >= 0 && translatedX < m_ImageWidth && translatedY >= 0 && translatedY < m_ImageHeight)
			{
				pImage->at<Vec4b>(y,x) = Vec4b(
				m_pColorBuffer[translatedY * m_colorBufferPitch + translatedX * 4 + 0],
                m_pColorBuffer[translatedY * m_colorBufferPitch + translatedX * 4 + 1],
                m_pColorBuffer[translatedY * m_colorBufferPitch + translatedX * 4 + 2],
                255);
			}
			else
			{
			pImage->at<Vec4b>(y,x) = Vec4b(
				m_pColorBuffer[y * m_colorBufferPitch + x * 4 + 0],
				m_pColorBuffer[y * m_colorBufferPitch + x * 4 + 1],
				m_pColorBuffer[y * m_colorBufferPitch + x * 4 + 2],
				255);
			}
        }
    }

    return S_OK;
}

HRESULT KinectDataStreamManager::GetDepthData(Mat* pImage)
{
	// Check if image is valid
	if (m_colorBufferPitch == 0)
	{
		return E_NUI_FRAME_NO_DATA;
	}

	DWORD depthHeight, depthWidth;
	GetDepthFrameSize(&depthWidth, &depthHeight);

	// Copy image information into Mat
	USHORT* pBufferRun = reinterpret_cast<USHORT*>(m_pDepthBuffer);

	for (UINT y = 0; y < depthHeight; ++y)
	{
		// Get row pointer for depth Mat
		USHORT* pDepthRow = pImage->ptr<USHORT>(y);

		for (UINT x = 0; x < depthWidth; ++x)
		{
			pDepthRow[x] = NuiDepthPixelToDepth(pBufferRun[y * depthWidth + x]);
		}
	}
	// Get transformation coordinates for color image
	HRESULT hr = m_pSensor->NuiImageGetColorPixelCoordinateFrameFromDepthPixelFrameAtResolution(
				COLOR_RESOLUTION,DEPTH_RESOLUTION,
				m_ImageHeight * m_ImageWidth,pBufferRun,
				m_ImageHeight * m_ImageWidth * 2,m_pColorCoordinates
				);
	m_bCompute = true;
	return S_OK;
}

Mat KinectDataStreamManager::GetColorImage()
{
	Mat tmp;
	WaitForSingleObject(m_hSynchronizationGetterMutex,INFINITE);
	m_ColorImage.copyTo(tmp);
	ReleaseMutex(m_hSynchronizationGetterMutex);
	return tmp;
}

Mat KinectDataStreamManager::GetDepthImage()
{
	Mat tmp;
	WaitForSingleObject(m_hSynchronizationGetterMutex,INFINITE);
	m_DepthImage.copyTo(tmp);
	ReleaseMutex(m_hSynchronizationGetterMutex);
	return tmp;
}


HRESULT KinectDataStreamManager::StartStream()
{
	if (m_pSensor) {
			m_bContinueStream = true;
			if (NULL != m_hImageStreamThread)
			{
				TerminateThread(m_hImageStreamThread,0);
				CloseHandle(m_hImageStreamThread);
			}
			m_hImageStreamThread = CreateThread(NULL, 0, StartStreamThread, this, 0, NULL);
			if (NULL != m_hImageStreamThread) {
				return S_OK;
			} else {
				return ERROR_THREAD_1_INACTIVE;
			}
	} 
	return ERROR_THREAD_ALREADY_IN_TASK;
}

DWORD WINAPI KinectDataStreamManager::StartStreamThread(LPVOID lpParam)
{
	KinectDataStreamManager *pThis = reinterpret_cast<KinectDataStreamManager*>(lpParam);
	return pThis->StreamThread();
}

void KinectDataStreamManager::StopStream()
{
	m_bContinueStream = false;
}


DWORD WINAPI KinectDataStreamManager::StreamThread()
{
	while(m_bContinueStream) 
	{	

		// Update color frame
		if (SUCCEEDED(UpdateColorFrame())) 
		{
			WaitForSingleObject(m_hImageStreamMutex, INFINITE);
			WaitForSingleObject(m_hSynchronizationGetterMutex,INFINITE);
			if (FAILED(GetColorData(&m_ColorImage)))
			{
				ReleaseMutex(m_hSynchronizationGetterMutex);
				ReleaseMutex(m_hImageStreamMutex);
				continue;
			}
			ReleaseMutex(m_hSynchronizationGetterMutex);
			ReleaseMutex(m_hImageStreamMutex);
		}
		else
		{
			continue;
		}

		// Update depth frame
		if (SUCCEEDED(UpdateDepthFrame())) 
		{
			WaitForSingleObject(m_hImageStreamMutex, INFINITE);
			WaitForSingleObject(m_hSynchronizationGetterMutex,INFINITE);
			if (FAILED(GetDepthData(&m_DepthImage)))
			{
				ReleaseMutex(m_hSynchronizationGetterMutex);
				ReleaseMutex(m_hImageStreamMutex);
				continue;
			}
			ReleaseMutex(m_hSynchronizationGetterMutex);
			ReleaseMutex(m_hImageStreamMutex);
		}
		else
		{
			continue;
		}

		ReleaseMutex(m_hImageStreamMutex);
		Sleep(33);
	}
	return 0;
}

HANDLE * KinectDataStreamManager::GetImageStreamMutex()
{
	return &m_hImageStreamMutex;
}
