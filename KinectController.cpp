#include "stdafx.h"
#include "KinectController.h"


KinectController::KinectController(void) :
	m_pSensor(NULL)
{
}


KinectController::~KinectController(void)
{
}

// Returns true if the Kinect has been initialized, false otherwised
bool KinectController::IsInitialized() const
{
	return m_pSensor != NULL;
}

HRESULT KinectController::InitializeFirstConnected() 
{
	HRESULT hr;
	INuiSensor * sensor = NULL;

	// Get number of Kinect sensors
	int sensorCount = 0;
	hr = NuiGetSensorCount(&sensorCount);
	if (FAILED(hr)) 
	{
		return hr;
	}

	// If no sensors, return failure
	if (0 == sensorCount) {
		return E_FAIL;
	}

	// Iterate through Kinect sensors until one is successfully initialized
	for (int i = 0; i < sensorCount; ++i) 
	{
		hr = NuiCreateSensorByIndex(i, &sensor);
		if (SUCCEEDED(hr))
		{
			hr = m_dataStreamManager.Initialize(sensor);
			if (SUCCEEDED(hr)) 
			{
				// Report success
				m_pSensor = sensor;
				// initialise sensor AngleManager
				if(!m_tiltAngleManager.IsInitialized())
				{
					m_tiltAngleManager.Initialize(sensor);
				}
				return S_OK;
			}
			else
			{
				// Uninitialize KinectHelper to show that Kinect is not ready
				m_dataStreamManager.UnInitialize();
			}
		}
	}

	// Report failure
	return E_FAIL;
}

HRESULT KinectController::GetAngle(long *degree) 
{
	if (m_tiltAngleManager.IsInitialized())
	{
		m_tiltAngleManager.GetElevationAngle(degree);
		return S_OK;
	} 
	else 
	{
		return ERROR_DEVICE_NOT_CONNECTED;
	}
}

HRESULT KinectController::ChangeAngle(long degree) 
{
	if (m_tiltAngleManager.IsInitialized()) 
	{
		m_tiltAngleManager.SetElevationAngle(degree);
		return S_OK;
	} 
	else 
	{
		return ERROR_DEVICE_NOT_CONNECTED;
	}
}

HRESULT KinectController::OpenStream()
{
	if (m_dataStreamManager.IsInitialized()) 
	{
		return m_dataStreamManager.StartStream();
	}
	return ERROR_DEVICE_NOT_AVAILABLE;
}

void KinectController::StopStream()
{
	m_dataStreamManager.StopStream();
}


KinectDataStreamManager* KinectController::GetKinectDataStreamManager()
{
	return &m_dataStreamManager;
}
