#include "stdafx.h"
#include "KinectPitch.h"


KinectPitch::KinectPitch(QWidget *parent)
{
	ui.setupUi(this);
}


KinectPitch::~KinectPitch(void)
{
}


void KinectPitch::SetActualAngle(long angle)
{
	m_ActualAngle = (long)angle;
	ui.pitchSlider->setValue(angle);
	ui.pitchValue->setValue(angle);
}


void KinectPitch::accept()
{
	m_ActualAngle = (long)ui.pitchValue->value();
	emit valueChanged(m_ActualAngle);
	this->hide();
}


void KinectPitch::reject()
{
	changeValue(m_ActualAngle);
	this->hide();
}


void KinectPitch::changeValue(int angle)
{
	emit valueChanged(angle);
}
