#include <opencv2\core\core.hpp>
#include <opencv2\opencv.hpp>
#include "Person.h"

#pragma once

using namespace cv;
class PeopleCounter : 
	public QObject
{
	Q_OBJECT

// Direction of people movement
//
#define DIRECTION_IN "in"
#define DIRECTION_OUT "out"

public:
	PeopleCounter(void);
	PeopleCounter(Point2f doorLine, Point2f areaLine);
	~PeopleCounter(void);

	enum Orientation
	{
		Y_AXIS,
		X_AXIS
	};

private:
	Point2f m_DoorLine;
	Point2f m_AreaLine;
	unsigned long m_Visiting;
	unsigned long m_InRoom;
	unsigned long m_Leaving;
	int m_Orientation;

	unsigned long m_Frames;
	unsigned int m_DeleteRate;

public:
	void SetLinesPositions(Point2f doorLine, Point2f areaLine);
	
	//Checks position of each detected person, counts visitors/leavers/non-defined and removes idle Persons
	void operator() (vector<Person> &detectedPeople);

	void ResetCounters();

	unsigned long GetVisitorsCount(void);
	unsigned long GetInRoomCount(void);
	unsigned long GetLeaversCount(void);
	void GetAllCounts(unsigned long &visitors, unsigned long &leavers, unsigned long &in_room);
	int GetOrientation();
	Point2f GetDoorLine();
	Point2f GetAreaLine();
	void GetLinesEndpoints(Point2f &doorEnd, Point2f &areaEnd);

public slots:
	void changeDoorLinePosition(Point2f door);
	void changeAreaLinePosition(Point2f area);
	void changeOrientation(bool orientation);
	void SetInRoomCounter(int people = 0);

signals:
	void peopleMovementNoted(std::string direction, int inRoom, SYSTEMTIME timestamp);
};

