BP 2015 - Hudec - Kinect People Counter
===============================================================================
**Author:** Bc. Lukáš Hudec  
**Bachelor thesis:** Visual detection of people flow with use of Kinect sensor  
**Supervisor:** Ing. Vanda Benešová, PhD.  
**Keywords:** people detection, segmentation, tracking, threshold, morphological reconstruction, kinect, opencv  
2015, May  

Thesis Description
===============================================================================
This paper deals with the problem of detection and tracking people in huge crowd with help of depth sensor and Kinect camera. Depth map from Kinect sensor is essential information for the solution of people detection problem. Sensor was situated above the entrance to a room so it scanned the vertical area and people flow underneath. Object detection is complex topic containing two main study areas - segmentation and tracking. First part is concerned with segmentation methods and their combination useful for people detection from depth images. The next part of analysis explains methods of object tracking between frames of video stream. Other works about problem of people detection on colorized or depth images are mentioned in the third part. Fourth and fifth part deals with the solution itself. Solution begins with the usage of basic segmentation techniques, comparing their time complexity and effectiveness. After that, main method of this work is implemented and its complexity and effectiveness is elaborated. The last part contains comparison with other methods and discussion about application of used methods in real-life problems and conditions.

------------------------------------------------------------------------------