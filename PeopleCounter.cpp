#include "stdafx.h"
#include "PeopleCounter.h"

#define DEFAULT_DELETE_RATE 100

PeopleCounter::PeopleCounter(void) :
	m_AreaLine(Point2f(0,180)),
	m_DoorLine(Point2f(0,300)),
	m_Visiting(0),
	m_InRoom(0),
	m_Leaving(0),
	m_Frames(0),
	m_DeleteRate(DEFAULT_DELETE_RATE),
	m_Orientation(Orientation::Y_AXIS)
{
}


PeopleCounter::PeopleCounter(Point2f doorLine, Point2f areaLine) :
	m_Visiting(0),
	m_InRoom(0),
	m_Leaving(0),
	m_Frames(0),
	m_DeleteRate(DEFAULT_DELETE_RATE)
{
	m_DoorLine = doorLine;
	m_AreaLine = areaLine;
	if (doorLine.y == 0 && doorLine.x > 0)
	{
		m_Orientation = Orientation::X_AXIS;
	}
}


PeopleCounter::~PeopleCounter(void)
{
}


void PeopleCounter::SetLinesPositions(Point2f doorLine, Point2f areaLine)
{
	m_DoorLine = doorLine;
	m_AreaLine = areaLine;

	if (doorLine.y == 0 && areaLine.y == 0)
	{
		m_Orientation = Orientation::X_AXIS;
	}

	m_Visiting = 0;
	m_InRoom = 0;
	m_Leaving = 0;
	m_Frames = 0;
}


void PeopleCounter::ResetCounters()
{
	m_Visiting = 0;
	m_InRoom = 0;
	m_Leaving = 0;
	m_Frames = 0;
}


void PeopleCounter::SetInRoomCounter(int people)
{
	m_InRoom = people;
}


unsigned long PeopleCounter::GetVisitorsCount(void)
{
	return m_Visiting;
}


unsigned long PeopleCounter::GetInRoomCount(void)
{
	return m_InRoom;
}


unsigned long PeopleCounter::GetLeaversCount(void)
{
	return m_Leaving;
}


void PeopleCounter::GetAllCounts(unsigned long &visitors, unsigned long &leavers, unsigned long &in_room)
{
	visitors = m_Visiting;
	in_room = m_InRoom;
	leavers = m_Leaving;
}


int PeopleCounter::GetOrientation()
{
	return m_Orientation;
}


Point2f PeopleCounter::GetAreaLine()
{
	return m_AreaLine;
}


Point2f PeopleCounter::GetDoorLine()
{
	return m_DoorLine;
}


void PeopleCounter::GetLinesEndpoints(Point2f &doorLine, Point2f &areaLine)
{
	if (m_Orientation == Orientation::Y_AXIS)
	{
		doorLine = m_DoorLine;
		areaLine = m_AreaLine;
		areaLine.x = doorLine.x = 640;
	}
	else if (m_Orientation == Orientation::X_AXIS)
	{
		doorLine = m_DoorLine;
		areaLine = m_AreaLine;
		areaLine.y = doorLine.y = 480;
	}

}


void PeopleCounter::changeDoorLinePosition(Point2f door)
{
	m_DoorLine = door;
}


void PeopleCounter::changeAreaLinePosition(Point2f area)
{
	m_AreaLine = area;
}


void PeopleCounter::changeOrientation(bool orientation)
{
	if (orientation)
	{
		m_Orientation = Orientation::X_AXIS;
	}
	else
	{
		m_Orientation = Orientation::Y_AXIS;
	}
}


void PeopleCounter::operator() (vector<Person> &detectedPeople)
{
	SYSTEMTIME timestamp;
	GetLocalTime(&timestamp);
	int door, area, previous, actual;
	if (m_Orientation == Orientation::X_AXIS)
	{
		door = m_DoorLine.x;
		area = m_AreaLine.x;
	}
	else
	{
		door = m_DoorLine.y;
		area = m_AreaLine.y;
	}
	for (int i=0; i < detectedPeople.size(); i++)
	{
		if (m_Orientation == Orientation::X_AXIS)
		{
			previous = detectedPeople[i].GetPreviousPosition().x;
			actual = detectedPeople[i].GetActualPosition().x;
		}
		else
		{
			previous = detectedPeople[i].GetPreviousPosition().y;
			actual = detectedPeople[i].GetActualPosition().y;
		}
		
		switch (detectedPeople[i].GetDirectionState())
		{
		case Person::Directions::STAYING :
			{
				if (previous == actual && door > actual && actual > area)
				{
					detectedPeople[i].SetDirectionState(Person::Directions::UNKNOWN);
				}
				if (previous <= area && area < actual)
				{
					detectedPeople[i].SetDirectionState(Person::Directions::LEAVING);
				}
				if (previous > door && door >= actual)
				{
					detectedPeople[i].SetDirectionState(Person::Directions::VISITED);
				}
				break;
			}
		case Person::Directions::VISITED :
			{
				if (previous >= area && area > actual)
				{
					detectedPeople[i].SetDirectionState(Person::Directions::ENTERED);
					m_Visiting++;
					m_InRoom++;
					static const QMetaMethod peoplemovement = QMetaMethod::fromSignal(&PeopleCounter::peopleMovementNoted);
					{
						if (isSignalConnected(peoplemovement))
						emit peopleMovementNoted(DIRECTION_IN,m_InRoom,timestamp);
					}

					
				}
				if (previous < door && door <= actual)
				{
					detectedPeople[i].SetDirectionState(Person::Directions::STAYING);
				}
				break;
			}
		case Person::Directions::ENTERED :
			{
				if (previous <= area && area < actual)
				{
					detectedPeople[i].SetDirectionState(Person::Directions::VISITED);
					m_Visiting--;
					m_InRoom--;
				}
				if (m_Frames % m_DeleteRate == 0)
				{
					detectedPeople.erase(detectedPeople.begin() + i);
					i--;
				}
				break;
			}
		case Person::Directions::LEAVING :
			{
				if (previous <= door && door < actual)
				{
					detectedPeople[i].SetDirectionState(Person::Directions::LEFT);
					m_Leaving++;
					if (m_InRoom > 0)
					{
						m_InRoom--;
					}

					emit peopleMovementNoted(DIRECTION_OUT,m_InRoom,timestamp);
				}
				if (previous >= area && area > actual)
				{
					detectedPeople[i].SetDirectionState(Person::Directions::STAYING);
				}
				break;
			}
		case Person::Directions::LEFT :
			{
				if (previous > door && door >= actual)
				{
					detectedPeople[i].SetDirectionState(Person::Directions::LEAVING);
					m_Leaving--;
					m_InRoom++;
				}
				if (m_Frames % m_DeleteRate == 0)
				{
					detectedPeople.erase(detectedPeople.begin() + i);
					i--;
				}
				break;
			}
		case Person::Directions::UNKNOWN :
			{
				if (previous <= door && door < actual)
				{
					detectedPeople[i].SetDirectionState(Person::Directions::LEFT);
					m_Leaving++;
					if (m_InRoom > 0)
					{
						m_InRoom--;
					}
					emit peopleMovementNoted(DIRECTION_OUT,m_InRoom,timestamp);
				}
				if (previous >= area && area > actual)
				{
					detectedPeople[i].SetDirectionState(Person::Directions::ENTERED);
					m_Visiting++;
					m_InRoom++;
					
					emit peopleMovementNoted(DIRECTION_IN,m_InRoom,timestamp);
				}
			}
		} // switch
	} // for
	m_Frames++;
}
