#include "stdafx.h"
#include "FileStreamManager.h"

#include <tchar.h>
#include <stdio.h>
#include <string.h>
#include <strsafe.h>
#include <Shlwapi.h>


FileStreamManager::FileStreamManager(void) : 
	m_bIsRecording(false),
	m_SourceDirectoryName("~/"),
	m_LogFileName("PeopleDetection")
{
	TCHAR buffer[MAXSHORT];
	GetCurrentDirectory(MAXSHORT,buffer);

	std::wstring widepath = std::wstring(buffer);
	m_PathToLoggingDirectory = std::string(widepath.begin(),widepath.end());
}

FileStreamManager::~FileStreamManager(void)
{
	m_bIsRecording = false;
	CloseLogFile();
}

void FileStreamManager::SetOutputDirectory(std::string directoryName)
{
	m_OutputFolderName = directoryName;
}

void FileStreamManager::SetSourceDirectory(const std::string directoryName)
{
	m_SourceDirectoryName = directoryName;
	WIN32_FIND_DATAA colorFFD, depthFFD;
	HANDLE hFindC = INVALID_HANDLE_VALUE, hFindD = INVALID_HANDLE_VALUE;

	hFindC = FindFirstFileA((directoryName + "\\Color\\c*").c_str(),&colorFFD);
	hFindD = FindFirstFileA((directoryName + "\\Depth\\d*").c_str(),&depthFFD);
	if (hFindC == INVALID_HANDLE_VALUE && hFindD == INVALID_HANDLE_VALUE)
	{
		return;
	}
	std::string tmp = colorFFD.cFileName;
	int underscore = tmp.find_last_of("_");
	int dot = tmp.find_last_of(".");
	m_ColorPrefix = tmp.substr(0,underscore+1);
	m_ColorSuffix = tmp.substr(dot,std::string::npos);
	m_ColorSourceOrder = std::atoi(tmp.substr(underscore+1,dot).c_str());

	tmp = depthFFD.cFileName;
	underscore = tmp.find_last_of("_");
	dot = tmp.find_last_of(".");
	m_DepthPrefix = tmp.substr(0,underscore+1);
	m_DepthSuffix = tmp.substr(dot,std::string::npos);
	m_DepthSourceOrder = std::atoi(tmp.substr(underscore+1,dot).c_str());
}

bool FileStreamManager::InitializeOutputDirectory()
{
	if (CreateDirectory(StringToWString(m_OutputFolderName).c_str(),NULL))
	{
		CreateDirectory(StringToWString(m_OutputFolderName + "/Color").c_str(),NULL);
		CreateDirectory(StringToWString(m_OutputFolderName + "/Depth").c_str(),NULL);
		return true;
	}
	else
	{
		DWORD err = GetLastError();
		if (err == ERROR_ALREADY_EXISTS)
		{
			CreateDirectory(StringToWString(m_OutputFolderName + "/Color").c_str(),NULL);
			CreateDirectory(StringToWString(m_OutputFolderName + "/Depth").c_str(),NULL);
			return true;
		}
		if (err == ERROR_PATH_NOT_FOUND)
		{
			return false;
		}
	}
	return false;
}

std::string FileStreamManager::GetOutputFolderName()
{
	return m_OutputFolderName;
}

std::string FileStreamManager::GetSourceDirectoryName()
{
	return m_SourceDirectoryName;
}

void FileStreamManager::StartRecording()
{
	m_ColorOutputOrder = 1;
	m_DephtOutputOrder = 1;
	m_bIsRecording = true;
	InitializeOutputDirectory();
}

void FileStreamManager::StopRecording()
{
	m_bIsRecording = false;
}

bool FileStreamManager::IsRecording()
{
	return m_bIsRecording;
}

bool FileStreamManager::GetNextColorImage(cv::Mat *image)
{
	std::string path = m_SourceDirectoryName + "/Color/" + m_ColorPrefix + std::to_string(m_ColorSourceOrder++) + m_ColorSuffix;

	cv::Mat tmp = cv::imread(path);
	if (tmp.empty())
	{
		return false;
	}
	else
	{
		cv::cvtColor(tmp,*image,cv::COLOR_RGB2BGR);
		return true;
	}
}

bool FileStreamManager::GetNextDepthImage(cv::Mat *image)
{
	std::string path = m_SourceDirectoryName + "/Depth/" + m_DepthPrefix + std::to_string(m_DepthSourceOrder++) + m_DepthSuffix;

	cv::Mat tmp = cv::imread(path,cv::IMREAD_ANYDEPTH);
	if (tmp.empty())
	{
		return false;
	}
	else
	{
		tmp.copyTo(*image);
		return true;
	}
}

HRESULT FileStreamManager::SetPathToLogDirectory(const std::string directoryName)
{
	if (PathFileExists(StringToWString(directoryName).c_str()))
	{
		m_PathToLoggingDirectory = directoryName;
		return S_OK;
	}
	return GetLastError();
}

// Returns S_OK if file was created, else ERROR_ALREADY_EXISTS(183) if file exists, but opens it anyways
HRESULT FileStreamManager::OpenLogFile()
{
	SYSTEMTIME localTime;
	GetLocalTime(&localTime);
	std::string day = localTime.wDay < 10 ? "0" + std::to_string(localTime.wDay) : std::to_string(localTime.wDay) ;
	std::string month = localTime.wMonth < 10 ? "0" + std::to_string(localTime.wMonth) : std::to_string(localTime.wMonth);
	std::string filename = m_LogFileName + "_" + day + month + std::to_string(localTime.wYear) + ".csv";
	std::string path = m_PathToLoggingDirectory + "/" + filename;
	
	// if file does not exists - write
	BOOL exists = PathFileExists(StringToWString(path).c_str());
	
	m_LogFileHandle = CreateFile(StringToWString(path).c_str(), FILE_APPEND_DATA | GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	
	HRESULT hr = GetLastError();
	if (!exists || hr == S_OK)
	{
		DWORD bytesWritten = 0;
		std::string header = "Direction of Tracked Person;Actually in room;TimeStamp\r\n";
		WriteFile(m_LogFileHandle, header.c_str(), header.length(), &bytesWritten, NULL);
		FlushFileBuffers(m_LogFileHandle);
	}

	return hr;
}

BOOL FileStreamManager::CloseLogFile()
{
	if (m_LogFileHandle)
	{
		FlushFileBuffers(m_LogFileHandle);
		HRESULT hr = CloseHandle(m_LogFileHandle);
		m_LogFileHandle = NULL;
		return hr;
	}
	return false;
}

std::string FileStreamManager::GetPathToLoggingDirectory()
{
	return m_PathToLoggingDirectory;
}

void FileStreamManager::saveColorImageFromMat(cv::Mat image)
{
	std::string tmpName = m_OutputFolderName + "/Color/color_" + std::to_string(m_ColorOutputOrder++) + ".png";

	cv::imwrite(tmpName,image);
}

void FileStreamManager::saveDepthImageFromMat(cv::Mat image)
{
	std::string tmpName = m_OutputFolderName + "/Depth/depth_" + std::to_string(m_DephtOutputOrder++) + ".png";
	
	cv::imwrite(tmpName,image);
}

void FileStreamManager::logRoomMovement(std::string direction, int inRoom, SYSTEMTIME timestamp)
{
	DWORD bytesWritten = 0;
	std::string timedate = std::to_string(timestamp.wHour) + ":" 
		+ std::to_string(timestamp.wMinute) + ":" 
		+ std::to_string(timestamp.wSecond) + " " 
		+ std::to_string(timestamp.wDay) + "."
		+ std::to_string(timestamp.wMonth) +  "."
		+ std::to_string(timestamp.wYear) + "\r\n";
	std::string textRowToWrite = direction + ";" + std::to_string(inRoom) + ";" + timedate;
	const char *text = textRowToWrite.c_str();

	WriteFile(m_LogFileHandle, text, strlen(text), &bytesWritten, NULL);
}
