#include "stdafx.h"
#include "Tracker.h"

#define MAXFRAMES 5
#define LINEWIDTH 1

Tracker::Tracker(void) :
	m_Method(TrackingMethods::FT_SIMPLEBLOB),
	m_QualityLevel(0.01),
	m_CornersMax(30),
	m_MinDistance(0.1),
	m_Initialized(false),
	m_PyrScale(0.75),
	m_Levels(3),
	m_WinSize(21),
	m_Iterations(3),
	m_PolyN(7),
	m_PolySigma(1.5),
	m_Flags(OPTFLOW_FARNEBACK_GAUSSIAN),
	m_RadicalCounter(0),
	m_Alpha(0.95),
	m_TrackedPeople(0),
	rng(0xFFFFFF),
	m_MaxThreshDistance(10000),
	m_MaxLostTrackDistance(60)
{
	m_BlobParams.minDistBetweenBlobs = 20.0f;
	m_BlobParams.filterByArea = true;
	m_BlobParams.filterByCircularity = false;
	m_BlobParams.filterByColor = false;
	m_BlobParams.filterByConvexity = false;
	m_BlobParams.filterByInertia = false;
	m_BlobParams.minArea = 500;
	m_BlobParams.maxArea = 15000;
	m_BlobParams.minThreshold = 0;
	m_BlobParams.maxThreshold = 2;
	m_BlobParams.thresholdStep = 1;
	m_BlobDetector = new SimpleBlobDetector(m_BlobParams);
	m_MaxFistHeadSize = 16;
	m_MaxFistHeadHeigth = 1500;
}


Tracker::~Tracker(void)
{
}


void Tracker::SetTrackingMethod(int method)
{
	m_Method = method;
	m_Initialized = false;
}


void Tracker::Initialize(Mat firstFrame, InputArray mask, InputArray heigthImage)
{
	m_TrackerMask = Mat::zeros(firstFrame.rows,firstFrame.cols,firstFrame.type());
	if (m_Method == TrackingMethods::FT_SIMPLEBLOB)
	{
		Mat image = mask.getMat();
		Mat heigthImg = heigthImage.getMat();

		m_BlobDetector->detect(image,m_PreviousKeyPoints);
		if (m_PreviousKeyPoints.size() > 0)
		{
//			drawKeypoints(firstFrame,m_PreviousKeyPoints,firstFrame,Scalar(0,255,30)); --- vykreslovanie keypointov nefunguje pri streame z Kinectu - dovod nejasny
			for (int i = 0; i < m_PreviousKeyPoints.size(); i++)
			{
				// Precheck for the head/fist problem
				USHORT heigth = heigthImg.at<USHORT>(m_PreviousKeyPoints[i].pt);
				float size = m_PreviousKeyPoints[i].size ;
				if (size <= m_MaxFistHeadSize)
				{
					if (heigth > m_MaxFistHeadHeigth)
					{
						continue;
					}
				}
				m_TrackedPeople.push_back(Person(m_PreviousKeyPoints[i].pt,RandomColor()));
			}
		}
		if (m_TrackedPeople.size() > 0)
		{
			m_Initialized = true;
		}
		image.copyTo(m_ImageBlobMaskPrevious);
	}
	else
	{
		cvtColor(firstFrame,m_ImagePrevious,CV_BGR2GRAY);
		goodFeaturesToTrack(m_ImagePrevious, m_FeaturesPrevious, m_CornersMax, m_QualityLevel, m_MinDistance, mask);
		if (m_FeaturesPrevious.size() > 0)
		{
			m_Initialized = true;
			for (int i = 0; i < m_FeaturesPrevious.size(); i++)
			{
				m_TrackedPeople.push_back(Person(m_FeaturesPrevious[i],RandomColor()));
			}
		}
	}
}


bool Tracker::IsInitialized()
{
	return m_Initialized;
}


void Tracker::Reset()
{
	m_Initialized = false;
	m_TrackerMask = Mat::zeros(m_TrackerMask.rows,m_TrackerMask.cols,m_TrackerMask.type());
	m_TrackedPeople.clear();
	m_GrayMask.release();
}


Mat Tracker::operator()(Mat nextFrame, InputArray mask, InputArray heigthImage)
{
	Mat imageMask = mask.getMat();
	Mat imageFrame;
	Mat grayscale;
	
	// why should i slow myself, when i don't have to
	if (m_Method != TrackingMethods::FT_SIMPLEBLOB)
	{
		imageFrame = grayMaskNextFrame(nextFrame,mask);
		cvtColor(imageFrame,grayscale,CV_BGR2GRAY);	
	}
	Mat output;
	std::vector<cv::Point2f> nextFeatures;
	switch (m_Method)
	{
	case TrackingMethods::OF_PYR_LK :
		{
			output = imageFrame;
			std::vector<uchar> status;
			std::vector<float> err;
			if (m_FeaturesPrevious.size() > 0)
			{
				calcOpticalFlowPyrLK(m_ImagePrevious,grayscale,m_FeaturesPrevious,nextFeatures,status,err);
				m_TrackerMask *= m_Alpha;
				for (int i=0; i < nextFeatures.size(); i++)
				{
					if (status[i])
					{
						line(m_TrackerMask,m_FeaturesPrevious[i],nextFeatures[i],CV_RGB(10,255,10),LINEWIDTH,4);
					}
				}
			}
			output += m_TrackerMask;
			break;
		}
	case TrackingMethods::OF_FARNEBACK :
		{
			output = imageFrame;
			Mat densityMat;
			calcOpticalFlowFarneback(m_ImagePrevious,grayscale,densityMat,m_PyrScale,m_Levels,m_WinSize,m_Iterations,m_PolyN,m_PolySigma,m_Flags);
			drawOptFlowMap(densityMat,output,16,1.5,CV_RGB(10,255,10));
			break;
		}
	case TrackingMethods::FT_SIMPLEBLOB:
		{
			output = nextFrame;
			Mat heigthImg = heigthImage.getMat();
			vector<KeyPoint> nextKeypoints;
			m_BlobDetector->detect(imageMask,nextKeypoints);
			m_TrackerMask *= m_Alpha;
			if (nextKeypoints.size() > 0)
			{
				vector<int> distances = matchTwoKeyPointVectors(m_PreviousKeyPoints,nextKeypoints);
				for (int i = 0; i < nextKeypoints.size(); i++)
				{
					if (i < m_PreviousKeyPoints.size())
					{
						bool newPerson = true;
						if (distances[i] < m_MaxThreshDistance)
						{							
							for (int j = 0; j < m_TrackedPeople.size(); j++)
							{
								if (m_TrackedPeople[j].GetActualPosition() == m_PreviousKeyPoints[i].pt)
								{
									newPerson = false;
									m_TrackedPeople[j].SetNextPosition(nextKeypoints[i].pt);
									line(m_TrackerMask,m_TrackedPeople[j].GetPreviousPosition(),m_TrackedPeople[j].GetActualPosition(),m_TrackedPeople[j].GetLineColor(),LINEWIDTH,4);
									break;
								}
							}
						}
						if (newPerson)	
						{
							m_TrackedPeople.push_back(Person(nextKeypoints[i].pt,RandomColor()));
						}
					}
					else
					{
						// Take a look around the point to m_MaxLostTrackDistance to see if you can find who is your predecessor
						bool newPerson = true;
						if (i < m_TrackedPeople.size())
						{
							for (int j = 0; j < m_TrackedPeople.size(); j++)
							{
								Point2f person = m_TrackedPeople[j].GetActualPosition();
								int distance = sqrt(pow(nextKeypoints[i].pt.x - person.x,2) + pow(nextKeypoints[i].pt.y - person.y,2));
								int direction = m_TrackedPeople[j].GetDirectionState();
								if ((direction==Person::Directions::STAYING || direction==Person::Directions::VISITED || direction==Person::Directions::LEAVING) && distance < m_MaxLostTrackDistance)
								{
									m_TrackedPeople[j].SetNextPosition(nextKeypoints[i].pt);
									line(m_TrackerMask,m_TrackedPeople[j].GetPreviousPosition(),m_TrackedPeople[j].GetActualPosition(),m_TrackedPeople[j].GetLineColor(),LINEWIDTH,4);
									newPerson = false;
									break;
								}
							}
						}
						if (newPerson)
						{
							// Precheck for the head/fist problem
							USHORT heigth = heigthImg.at<USHORT>(nextKeypoints[i].pt);
							float size = nextKeypoints[i].size;
							if (size <= m_MaxFistHeadSize)
							{
								if (heigth > m_MaxFistHeadHeigth)
								{
									continue;
								}
							}
							m_TrackedPeople.push_back(Person(nextKeypoints[i].pt,RandomColor()));
						}
					}
				}
//				drawKeypoints(output,nextKeypoints,output,CV_RGB(10,255,30)); --- vykreslovanie keypointov nefunguje pri streame z Kinectu - dovod nejasny
			}
			addWeighted(output,1,m_TrackerMask,1,10,output);
			m_PreviousKeyPoints = nextKeypoints;
			imageMask.copyTo(m_ImageBlobMaskPrevious);
			break;
		}
	}

	grayscale.copyTo(m_ImagePrevious);
	m_FeaturesPrevious = nextFeatures;
	m_RadicalCounter++;
	if (m_Method == TrackingMethods::OF_PYR_LK && m_RadicalCounter > MAXFRAMES)
	{
		m_RadicalCounter = 0;
		cvtColor(imageFrame,m_ImagePrevious,CV_BGR2GRAY);
		goodFeaturesToTrack(m_ImagePrevious, m_FeaturesPrevious, m_CornersMax, m_QualityLevel, m_MinDistance, mask);
		if (nextFeatures.size() == 0)
		{
			m_Initialized = false;
		}
	}
	return output;
}


void Tracker::drawOptFlowMap(const Mat& flow, Mat& cflowmap, int step, double scale, const Scalar& color)
{
	for(int y = 0; y < cflowmap.rows; y += step)
		for(int x = 0; x < cflowmap.cols; x += step)
		{
			const Point2f& fxy = flow.at<Point2f>(y, x);
			line(cflowmap, Point(x,y), Point(cvRound(x+fxy.x), cvRound(y+fxy.y)), color);
			circle(cflowmap, Point(x,y), 2, color, -1);
		}
}


Mat Tracker::grayMaskNextFrame(InputArray nextFrame, InputArray mask)
{
	Mat color = nextFrame.getMat();
	Mat imageMask = mask.getMat();

	if (m_GrayMask.empty())
	{
		m_GrayMask = Mat(color.rows,color.cols,color.type(),Scalar(127,127,127));
	}
	Mat invertedMask;
	invertedMask = 255 - imageMask;
	Mat tmp;
	Mat tmp2;
	color.copyTo(tmp,imageMask);
	m_GrayMask.copyTo(tmp2,invertedMask);
	return tmp + tmp2;
}
