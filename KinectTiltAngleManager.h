#pragma once
#include <Windows.h>
#include <NuiApi.h>
#include "Utility.h"

class KinectTiltAngleManager
{
public:
	KinectTiltAngleManager(void);
	~KinectTiltAngleManager(void);
private:
	INuiSensor *m_pSensor;
	LONG m_tiltAngle;
	HANDLE m_hSetTiltAngleEvent;
	HANDLE m_hExitThreadEvent;

	HANDLE m_hElevationTaskThread;
public:
	void Initialize(INuiSensor *pSensor);
	bool IsInitialized();
	void GetElevationAngle(LONG *angle);
	void SetElevationAngle(LONG angle);
	void StartElevationThread(void);
	static DWORD WINAPI ElevationThread(KinectTiltAngleManager *pThis);
};

