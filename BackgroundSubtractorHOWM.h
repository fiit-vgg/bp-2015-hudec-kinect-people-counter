#pragma once
#include "opencv2\video\background_segm.hpp"

// History of Weighted Medians
class BackgroundSubtractorHOWM :
	public cv::BackgroundSubtractor
{
public:
	BackgroundSubtractorHOWM(int noiseThresh = 0);
	~BackgroundSubtractorHOWM(void);

	// Computes a foreground mask.
	virtual void operator()(cv::Mat image, cv::Mat &fgmask, double learningRate=0);
	// Computes a background image.
    virtual void getBackgroundImage(cv::Mat &backgroundImage) const;
	// Returns medians Matrix
	void getMediansMatrix(cv::Mat &medianMatrix);
	// Resets history and matrices
	void initialize(int noiseThres);

private:
	long m_history;
	USHORT m_noiseThreshold;
	cv::Mat m_MatrixOfMedians;
	cv::Mat m_BackgroundImage;
};

