/****************************************************************************
** Meta object code from reading C++ file 'PeopleDetector.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.3.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "stdafx.h"
#include "../../PeopleDetector.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'PeopleDetector.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.3.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_PeopleDetector_t {
    QByteArrayData data[33];
    char stringdata[522];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_PeopleDetector_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_PeopleDetector_t qt_meta_stringdata_PeopleDetector = {
    {
QT_MOC_LITERAL(0, 0, 14),
QT_MOC_LITERAL(1, 15, 21),
QT_MOC_LITERAL(2, 37, 0),
QT_MOC_LITERAL(3, 38, 23),
QT_MOC_LITERAL(4, 62, 25),
QT_MOC_LITERAL(5, 88, 23),
QT_MOC_LITERAL(6, 112, 11),
QT_MOC_LITERAL(7, 124, 13),
QT_MOC_LITERAL(8, 138, 17),
QT_MOC_LITERAL(9, 156, 20),
QT_MOC_LITERAL(10, 177, 18),
QT_MOC_LITERAL(11, 196, 7),
QT_MOC_LITERAL(12, 204, 16),
QT_MOC_LITERAL(13, 221, 16),
QT_MOC_LITERAL(14, 238, 15),
QT_MOC_LITERAL(15, 254, 19),
QT_MOC_LITERAL(16, 274, 23),
QT_MOC_LITERAL(17, 298, 20),
QT_MOC_LITERAL(18, 319, 18),
QT_MOC_LITERAL(19, 338, 16),
QT_MOC_LITERAL(20, 355, 15),
QT_MOC_LITERAL(21, 371, 19),
QT_MOC_LITERAL(22, 391, 27),
QT_MOC_LITERAL(23, 419, 17),
QT_MOC_LITERAL(24, 437, 10),
QT_MOC_LITERAL(25, 448, 4),
QT_MOC_LITERAL(26, 453, 8),
QT_MOC_LITERAL(27, 462, 8),
QT_MOC_LITERAL(28, 471, 7),
QT_MOC_LITERAL(29, 479, 16),
QT_MOC_LITERAL(30, 496, 4),
QT_MOC_LITERAL(31, 501, 14),
QT_MOC_LITERAL(32, 516, 5)
    },
    "PeopleDetector\0on_playPshBtn_clicked\0"
    "\0on_recordPshBtn_clicked\0"
    "on_openFilePshBtn_clicked\0"
    "on_detectPshBtn_clicked\0exitProgram\0"
    "connectKinect\0changeKinectPitch\0"
    "kinectStreamActivate\0fileStreamActivate\0"
    "checked\0openSourceFolder\0openOutputFolder\0"
    "actionThreshold\0actionThresholdOTSU\0"
    "actionAdaptiveThreshold\0actionReconstruction\0"
    "actionLucas_Kanade\0actionSimpleBlob\0"
    "actionFarneback\0actionResetCounters\0"
    "actionPeopleCounterSettings\0"
    "actionSetDataPath\0showImages\0Mat*\0"
    "colorMat\0depthMat\0counter\0fileStreamStatus\0"
    "text\0setKinectPitch\0angle"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PeopleDetector[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      24,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  134,    2, 0x08 /* Private */,
       3,    0,  135,    2, 0x08 /* Private */,
       4,    0,  136,    2, 0x08 /* Private */,
       5,    0,  137,    2, 0x08 /* Private */,
       6,    0,  138,    2, 0x08 /* Private */,
       7,    0,  139,    2, 0x08 /* Private */,
       8,    0,  140,    2, 0x08 /* Private */,
       9,    0,  141,    2, 0x08 /* Private */,
      10,    1,  142,    2, 0x08 /* Private */,
      12,    0,  145,    2, 0x08 /* Private */,
      13,    0,  146,    2, 0x08 /* Private */,
      14,    0,  147,    2, 0x08 /* Private */,
      15,    0,  148,    2, 0x08 /* Private */,
      16,    0,  149,    2, 0x08 /* Private */,
      17,    0,  150,    2, 0x08 /* Private */,
      18,    0,  151,    2, 0x08 /* Private */,
      19,    0,  152,    2, 0x08 /* Private */,
      20,    0,  153,    2, 0x08 /* Private */,
      21,    0,  154,    2, 0x08 /* Private */,
      22,    0,  155,    2, 0x08 /* Private */,
      23,    0,  156,    2, 0x08 /* Private */,
      24,    3,  157,    2, 0x0a /* Public */,
      29,    1,  164,    2, 0x0a /* Public */,
      31,    1,  167,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   11,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 25, 0x80000000 | 25, QMetaType::Long,   26,   27,   28,
    QMetaType::Void, QMetaType::QString,   30,
    QMetaType::Void, QMetaType::Int,   32,

       0        // eod
};

void PeopleDetector::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        PeopleDetector *_t = static_cast<PeopleDetector *>(_o);
        switch (_id) {
        case 0: _t->on_playPshBtn_clicked(); break;
        case 1: _t->on_recordPshBtn_clicked(); break;
        case 2: _t->on_openFilePshBtn_clicked(); break;
        case 3: _t->on_detectPshBtn_clicked(); break;
        case 4: _t->exitProgram(); break;
        case 5: _t->connectKinect(); break;
        case 6: _t->changeKinectPitch(); break;
        case 7: _t->kinectStreamActivate(); break;
        case 8: _t->fileStreamActivate((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 9: _t->openSourceFolder(); break;
        case 10: _t->openOutputFolder(); break;
        case 11: _t->actionThreshold(); break;
        case 12: _t->actionThresholdOTSU(); break;
        case 13: _t->actionAdaptiveThreshold(); break;
        case 14: _t->actionReconstruction(); break;
        case 15: _t->actionLucas_Kanade(); break;
        case 16: _t->actionSimpleBlob(); break;
        case 17: _t->actionFarneback(); break;
        case 18: _t->actionResetCounters(); break;
        case 19: _t->actionPeopleCounterSettings(); break;
        case 20: _t->actionSetDataPath(); break;
        case 21: _t->showImages((*reinterpret_cast< Mat*(*)>(_a[1])),(*reinterpret_cast< Mat*(*)>(_a[2])),(*reinterpret_cast< long(*)>(_a[3]))); break;
        case 22: _t->fileStreamStatus((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 23: _t->setKinectPitch((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObject PeopleDetector::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_PeopleDetector.data,
      qt_meta_data_PeopleDetector,  qt_static_metacall, 0, 0}
};


const QMetaObject *PeopleDetector::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PeopleDetector::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_PeopleDetector.stringdata))
        return static_cast<void*>(const_cast< PeopleDetector*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int PeopleDetector::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 24)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 24;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 24)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 24;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
