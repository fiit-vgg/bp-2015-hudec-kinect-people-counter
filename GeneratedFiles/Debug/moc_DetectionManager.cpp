/****************************************************************************
** Meta object code from reading C++ file 'DetectionManager.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.3.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "stdafx.h"
#include "../../DetectionManager.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'DetectionManager.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.3.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_DetectionManager_t {
    QByteArrayData data[9];
    char stringdata[81];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_DetectionManager_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_DetectionManager_t qt_meta_stringdata_DetectionManager = {
    {
QT_MOC_LITERAL(0, 0, 16),
QT_MOC_LITERAL(1, 17, 12),
QT_MOC_LITERAL(2, 30, 0),
QT_MOC_LITERAL(3, 31, 4),
QT_MOC_LITERAL(4, 36, 8),
QT_MOC_LITERAL(5, 45, 8),
QT_MOC_LITERAL(6, 54, 7),
QT_MOC_LITERAL(7, 62, 13),
QT_MOC_LITERAL(8, 76, 4)
    },
    "DetectionManager\0imagesLoaded\0\0Mat*\0"
    "colorMat\0depthMat\0counter\0statusChanged\0"
    "text"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_DetectionManager[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    3,   24,    2, 0x06 /* Public */,
       7,    1,   31,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3, 0x80000000 | 3, QMetaType::Long,    4,    5,    6,
    QMetaType::Void, QMetaType::QString,    8,

       0        // eod
};

void DetectionManager::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        DetectionManager *_t = static_cast<DetectionManager *>(_o);
        switch (_id) {
        case 0: _t->imagesLoaded((*reinterpret_cast< Mat*(*)>(_a[1])),(*reinterpret_cast< Mat*(*)>(_a[2])),(*reinterpret_cast< long(*)>(_a[3]))); break;
        case 1: _t->statusChanged((*reinterpret_cast< QString(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (DetectionManager::*_t)(Mat * , Mat * , long );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&DetectionManager::imagesLoaded)) {
                *result = 0;
            }
        }
        {
            typedef void (DetectionManager::*_t)(QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&DetectionManager::statusChanged)) {
                *result = 1;
            }
        }
    }
}

const QMetaObject DetectionManager::staticMetaObject = {
    { &QThread::staticMetaObject, qt_meta_stringdata_DetectionManager.data,
      qt_meta_data_DetectionManager,  qt_static_metacall, 0, 0}
};


const QMetaObject *DetectionManager::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *DetectionManager::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_DetectionManager.stringdata))
        return static_cast<void*>(const_cast< DetectionManager*>(this));
    return QThread::qt_metacast(_clname);
}

int DetectionManager::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QThread::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 2)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 2;
    }
    return _id;
}

// SIGNAL 0
void DetectionManager::imagesLoaded(Mat * _t1, Mat * _t2, long _t3)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void DetectionManager::statusChanged(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_END_MOC_NAMESPACE
