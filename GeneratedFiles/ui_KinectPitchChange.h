/********************************************************************************
** Form generated from reading UI file 'KinectPitchChange.ui'
**
** Created by: Qt User Interface Compiler version 5.3.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_KINECTPITCHCHANGE_H
#define UI_KINECTPITCHCHANGE_H

#include <QtCore/QLocale>
#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_PitchDialog
{
public:
    QWidget *layoutWidget;
    QHBoxLayout *hboxLayout;
    QSpacerItem *spacerItem;
    QPushButton *okButton;
    QPushButton *cancelButton;
    QSlider *pitchSlider;
    QLabel *helpLabel;
    QLabel *sliderLabel_1;
    QLabel *sliderLabel_2;
    QLabel *sliderLabel_0;
    QSpinBox *pitchValue;

    void setupUi(QDialog *PitchDialog)
    {
        if (PitchDialog->objectName().isEmpty())
            PitchDialog->setObjectName(QStringLiteral("PitchDialog"));
        PitchDialog->setEnabled(true);
        PitchDialog->resize(300, 200);
        PitchDialog->setMaximumSize(QSize(300, 200));
        PitchDialog->setLocale(QLocale(QLocale::English, QLocale::UnitedStates));
        layoutWidget = new QWidget(PitchDialog);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(10, 160, 281, 33));
        hboxLayout = new QHBoxLayout(layoutWidget);
        hboxLayout->setSpacing(6);
        hboxLayout->setObjectName(QStringLiteral("hboxLayout"));
        hboxLayout->setContentsMargins(0, 0, 0, 0);
        spacerItem = new QSpacerItem(131, 31, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout->addItem(spacerItem);

        okButton = new QPushButton(layoutWidget);
        okButton->setObjectName(QStringLiteral("okButton"));

        hboxLayout->addWidget(okButton);

        cancelButton = new QPushButton(layoutWidget);
        cancelButton->setObjectName(QStringLiteral("cancelButton"));

        hboxLayout->addWidget(cancelButton);

        pitchSlider = new QSlider(PitchDialog);
        pitchSlider->setObjectName(QStringLiteral("pitchSlider"));
        pitchSlider->setGeometry(QRect(60, 10, 19, 135));
        pitchSlider->setCursor(QCursor(Qt::ArrowCursor));
        pitchSlider->setMouseTracking(false);
        pitchSlider->setAutoFillBackground(false);
        pitchSlider->setMinimum(-27);
        pitchSlider->setMaximum(27);
        pitchSlider->setPageStep(3);
        pitchSlider->setOrientation(Qt::Vertical);
        pitchSlider->setInvertedAppearance(false);
        pitchSlider->setInvertedControls(false);
        pitchSlider->setTickPosition(QSlider::TicksAbove);
        pitchSlider->setTickInterval(3);
        helpLabel = new QLabel(PitchDialog);
        helpLabel->setObjectName(QStringLiteral("helpLabel"));
        helpLabel->setEnabled(true);
        helpLabel->setGeometry(QRect(180, 50, 101, 41));
        helpLabel->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        helpLabel->setWordWrap(true);
        sliderLabel_1 = new QLabel(PitchDialog);
        sliderLabel_1->setObjectName(QStringLiteral("sliderLabel_1"));
        sliderLabel_1->setGeometry(QRect(15, 10, 35, 20));
        sliderLabel_1->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        sliderLabel_2 = new QLabel(PitchDialog);
        sliderLabel_2->setObjectName(QStringLiteral("sliderLabel_2"));
        sliderLabel_2->setGeometry(QRect(15, 120, 35, 20));
        sliderLabel_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        sliderLabel_0 = new QLabel(PitchDialog);
        sliderLabel_0->setObjectName(QStringLiteral("sliderLabel_0"));
        sliderLabel_0->setGeometry(QRect(15, 67, 35, 21));
        sliderLabel_0->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        pitchValue = new QSpinBox(PitchDialog);
        pitchValue->setObjectName(QStringLiteral("pitchValue"));
        pitchValue->setGeometry(QRect(90, 66, 45, 22));
        pitchValue->setMinimum(-27);
        pitchValue->setMaximum(27);
        pitchValue->setDisplayIntegerBase(10);

        retranslateUi(PitchDialog);
        QObject::connect(cancelButton, SIGNAL(clicked()), PitchDialog, SLOT(reject()));
        QObject::connect(pitchSlider, SIGNAL(valueChanged(int)), pitchValue, SLOT(setValue(int)));
        QObject::connect(pitchValue, SIGNAL(valueChanged(int)), pitchSlider, SLOT(setValue(int)));
        QObject::connect(pitchSlider, SIGNAL(valueChanged(int)), PitchDialog, SLOT(changeValue(int)));
        QObject::connect(okButton, SIGNAL(clicked()), PitchDialog, SLOT(accept()));

        QMetaObject::connectSlotsByName(PitchDialog);
    } // setupUi

    void retranslateUi(QDialog *PitchDialog)
    {
        PitchDialog->setWindowTitle(QApplication::translate("PitchDialog", "Change Kinect Pitch", 0));
        okButton->setText(QApplication::translate("PitchDialog", "OK", 0));
        cancelButton->setText(QApplication::translate("PitchDialog", "Cancel", 0));
        helpLabel->setText(QApplication::translate("PitchDialog", "Move slider to change Kinect Pitch", 0));
        sliderLabel_1->setText(QApplication::translate("PitchDialog", "27", 0));
        sliderLabel_2->setText(QApplication::translate("PitchDialog", "-27", 0));
        sliderLabel_0->setText(QApplication::translate("PitchDialog", "0", 0));
    } // retranslateUi

};

namespace Ui {
    class PitchDialog: public Ui_PitchDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_KINECTPITCHCHANGE_H
