/****************************************************************************
** Meta object code from reading C++ file 'FileStreamManager.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.3.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "stdafx.h"
#include "../../FileStreamManager.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'FileStreamManager.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.3.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_FileStreamManager_t {
    QByteArrayData data[12];
    char stringdata[143];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_FileStreamManager_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_FileStreamManager_t qt_meta_stringdata_FileStreamManager = {
    {
QT_MOC_LITERAL(0, 0, 17),
QT_MOC_LITERAL(1, 18, 21),
QT_MOC_LITERAL(2, 40, 0),
QT_MOC_LITERAL(3, 41, 7),
QT_MOC_LITERAL(4, 49, 5),
QT_MOC_LITERAL(5, 55, 21),
QT_MOC_LITERAL(6, 77, 15),
QT_MOC_LITERAL(7, 93, 11),
QT_MOC_LITERAL(8, 105, 9),
QT_MOC_LITERAL(9, 115, 6),
QT_MOC_LITERAL(10, 122, 10),
QT_MOC_LITERAL(11, 133, 9)
    },
    "FileStreamManager\0saveColorImageFromMat\0"
    "\0cv::Mat\0image\0saveDepthImageFromMat\0"
    "logRoomMovement\0std::string\0direction\0"
    "inRoom\0SYSTEMTIME\0timestamp"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_FileStreamManager[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   29,    2, 0x0a /* Public */,
       5,    1,   32,    2, 0x0a /* Public */,
       6,    3,   35,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 7, QMetaType::Int, 0x80000000 | 10,    8,    9,   11,

       0        // eod
};

void FileStreamManager::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        FileStreamManager *_t = static_cast<FileStreamManager *>(_o);
        switch (_id) {
        case 0: _t->saveColorImageFromMat((*reinterpret_cast< cv::Mat(*)>(_a[1]))); break;
        case 1: _t->saveDepthImageFromMat((*reinterpret_cast< cv::Mat(*)>(_a[1]))); break;
        case 2: _t->logRoomMovement((*reinterpret_cast< std::string(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< SYSTEMTIME(*)>(_a[3]))); break;
        default: ;
        }
    }
}

const QMetaObject FileStreamManager::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_FileStreamManager.data,
      qt_meta_data_FileStreamManager,  qt_static_metacall, 0, 0}
};


const QMetaObject *FileStreamManager::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *FileStreamManager::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_FileStreamManager.stringdata))
        return static_cast<void*>(const_cast< FileStreamManager*>(this));
    return QObject::qt_metacast(_clname);
}

int FileStreamManager::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 3)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 3;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
