/********************************************************************************
** Form generated from reading UI file 'PeopleCounterSettings.ui'
**
** Created by: Qt User Interface Compiler version 5.3.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PEOPLECOUNTERSETTINGS_H
#define UI_PEOPLECOUNTERSETTINGS_H

#include <QtCore/QLocale>
#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_CounterSettingsDialog
{
public:
    QWidget *layoutWidget;
    QHBoxLayout *hboxLayout;
    QSpacerItem *spacerItem;
    QPushButton *okButton;
    QPushButton *cancelButton;
    QWidget *formLayoutWidget;
    QFormLayout *formLayout_2;
    QLabel *peopleInRoomLbl;
    QSpinBox *peopleInRoomSpinBox;
    QLabel *doorLbl;
    QLabel *areaLbl;
    QSpinBox *doorSpinBox;
    QSpinBox *areaSpinBox;
    QLabel *directionLbl;
    QCheckBox *orientationVertical;

    void setupUi(QDialog *CounterSettingsDialog)
    {
        if (CounterSettingsDialog->objectName().isEmpty())
            CounterSettingsDialog->setObjectName(QStringLiteral("CounterSettingsDialog"));
        CounterSettingsDialog->resize(300, 200);
        CounterSettingsDialog->setLocale(QLocale(QLocale::English, QLocale::UnitedStates));
        layoutWidget = new QWidget(CounterSettingsDialog);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(10, 160, 281, 33));
        hboxLayout = new QHBoxLayout(layoutWidget);
        hboxLayout->setSpacing(6);
        hboxLayout->setObjectName(QStringLiteral("hboxLayout"));
        hboxLayout->setContentsMargins(0, 0, 0, 0);
        spacerItem = new QSpacerItem(120, 31, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout->addItem(spacerItem);

        okButton = new QPushButton(layoutWidget);
        okButton->setObjectName(QStringLiteral("okButton"));

        hboxLayout->addWidget(okButton);

        cancelButton = new QPushButton(layoutWidget);
        cancelButton->setObjectName(QStringLiteral("cancelButton"));

        hboxLayout->addWidget(cancelButton);

        formLayoutWidget = new QWidget(CounterSettingsDialog);
        formLayoutWidget->setObjectName(QStringLiteral("formLayoutWidget"));
        formLayoutWidget->setGeometry(QRect(10, 10, 281, 141));
        formLayout_2 = new QFormLayout(formLayoutWidget);
        formLayout_2->setObjectName(QStringLiteral("formLayout_2"));
        formLayout_2->setFieldGrowthPolicy(QFormLayout::FieldsStayAtSizeHint);
        formLayout_2->setRowWrapPolicy(QFormLayout::DontWrapRows);
        formLayout_2->setLabelAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        formLayout_2->setFormAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        formLayout_2->setContentsMargins(0, 0, 20, 0);
        peopleInRoomLbl = new QLabel(formLayoutWidget);
        peopleInRoomLbl->setObjectName(QStringLiteral("peopleInRoomLbl"));
        peopleInRoomLbl->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        formLayout_2->setWidget(0, QFormLayout::LabelRole, peopleInRoomLbl);

        peopleInRoomSpinBox = new QSpinBox(formLayoutWidget);
        peopleInRoomSpinBox->setObjectName(QStringLiteral("peopleInRoomSpinBox"));
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(peopleInRoomSpinBox->sizePolicy().hasHeightForWidth());
        peopleInRoomSpinBox->setSizePolicy(sizePolicy);
        peopleInRoomSpinBox->setMinimumSize(QSize(70, 0));
        peopleInRoomSpinBox->setLayoutDirection(Qt::LeftToRight);
        peopleInRoomSpinBox->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        peopleInRoomSpinBox->setMaximum(1000000);
        peopleInRoomSpinBox->setDisplayIntegerBase(10);

        formLayout_2->setWidget(0, QFormLayout::FieldRole, peopleInRoomSpinBox);

        doorLbl = new QLabel(formLayoutWidget);
        doorLbl->setObjectName(QStringLiteral("doorLbl"));

        formLayout_2->setWidget(2, QFormLayout::LabelRole, doorLbl);

        areaLbl = new QLabel(formLayoutWidget);
        areaLbl->setObjectName(QStringLiteral("areaLbl"));

        formLayout_2->setWidget(3, QFormLayout::LabelRole, areaLbl);

        doorSpinBox = new QSpinBox(formLayoutWidget);
        doorSpinBox->setObjectName(QStringLiteral("doorSpinBox"));
        sizePolicy.setHeightForWidth(doorSpinBox->sizePolicy().hasHeightForWidth());
        doorSpinBox->setSizePolicy(sizePolicy);
        doorSpinBox->setMinimumSize(QSize(70, 0));
        doorSpinBox->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        doorSpinBox->setMaximum(480);
        doorSpinBox->setSingleStep(5);

        formLayout_2->setWidget(2, QFormLayout::FieldRole, doorSpinBox);

        areaSpinBox = new QSpinBox(formLayoutWidget);
        areaSpinBox->setObjectName(QStringLiteral("areaSpinBox"));
        sizePolicy.setHeightForWidth(areaSpinBox->sizePolicy().hasHeightForWidth());
        areaSpinBox->setSizePolicy(sizePolicy);
        areaSpinBox->setMinimumSize(QSize(70, 0));
        areaSpinBox->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        areaSpinBox->setMaximum(480);
        areaSpinBox->setSingleStep(5);

        formLayout_2->setWidget(3, QFormLayout::FieldRole, areaSpinBox);

        directionLbl = new QLabel(formLayoutWidget);
        directionLbl->setObjectName(QStringLiteral("directionLbl"));

        formLayout_2->setWidget(1, QFormLayout::LabelRole, directionLbl);

        orientationVertical = new QCheckBox(formLayoutWidget);
        orientationVertical->setObjectName(QStringLiteral("orientationVertical"));
        orientationVertical->setLayoutDirection(Qt::RightToLeft);
        orientationVertical->setAutoExclusive(false);

        formLayout_2->setWidget(1, QFormLayout::FieldRole, orientationVertical);


        retranslateUi(CounterSettingsDialog);
        QObject::connect(okButton, SIGNAL(clicked()), CounterSettingsDialog, SLOT(accept()));
        QObject::connect(cancelButton, SIGNAL(clicked()), CounterSettingsDialog, SLOT(reject()));
        QObject::connect(doorSpinBox, SIGNAL(valueChanged(int)), CounterSettingsDialog, SLOT(changeDoorLinePosition(int)));
        QObject::connect(areaSpinBox, SIGNAL(valueChanged(int)), CounterSettingsDialog, SLOT(changeAreaLinePosition(int)));
        QObject::connect(areaSpinBox, SIGNAL(valueChanged(int)), CounterSettingsDialog, SLOT(changeDoorMin(int)));
        QObject::connect(doorSpinBox, SIGNAL(valueChanged(int)), CounterSettingsDialog, SLOT(changeAreaMax(int)));
        QObject::connect(orientationVertical, SIGNAL(clicked(bool)), CounterSettingsDialog, SLOT(orientationBoxClicked(bool)));

        QMetaObject::connectSlotsByName(CounterSettingsDialog);
    } // setupUi

    void retranslateUi(QDialog *CounterSettingsDialog)
    {
        CounterSettingsDialog->setWindowTitle(QApplication::translate("CounterSettingsDialog", "People Counter Settings", 0));
        okButton->setText(QApplication::translate("CounterSettingsDialog", "OK", 0));
        cancelButton->setText(QApplication::translate("CounterSettingsDialog", "Cancel", 0));
        peopleInRoomLbl->setText(QApplication::translate("CounterSettingsDialog", "Initial people in the room", 0));
        doorLbl->setText(QApplication::translate("CounterSettingsDialog", "Door Line position", 0));
        areaLbl->setText(QApplication::translate("CounterSettingsDialog", "Area Line Position", 0));
        directionLbl->setText(QApplication::translate("CounterSettingsDialog", "Orientation -", 0));
        orientationVertical->setText(QApplication::translate("CounterSettingsDialog", "vertically", 0));
    } // retranslateUi

};

namespace Ui {
    class CounterSettingsDialog: public Ui_CounterSettingsDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PEOPLECOUNTERSETTINGS_H
