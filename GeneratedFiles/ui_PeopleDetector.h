/********************************************************************************
** Form generated from reading UI file 'PeopleDetector.ui'
**
** Created by: Qt User Interface Compiler version 5.3.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PEOPLEDETECTOR_H
#define UI_PEOPLEDETECTOR_H

#include <QtCore/QLocale>
#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_PeopleDetectorClass
{
public:
    QAction *actionConnect;
    QAction *actionChange_Pitch;
    QAction *actionFrom_Kinect;
    QAction *actionFrom_File;
    QAction *actionOpen_File;
    QAction *actionExit;
    QAction *actionSource_folder;
    QAction *actionOutput_folder;
    QAction *actionThreshold;
    QAction *actionThreshold_OTSU;
    QAction *actionReconstruction;
    QAction *actionThreshold_Adaptive;
    QAction *actionLucas_Kanade;
    QAction *actionFarneback;
    QAction *actionSimpleBlob;
    QAction *actionReset_Counters;
    QAction *actionSetPeopleCounter;
    QAction *actionDataPath;
    QWidget *centralWidget;
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *toolBox;
    QPushButton *playPshBtn;
    QPushButton *detectPshBtn;
    QLabel *threshLbl;
    QSpinBox *threshVal;
    QPushButton *recordPshBtn;
    QLabel *outputLbl;
    QLineEdit *outputPath;
    QPushButton *openFilePshBtn;
    QLabel *counterLbl;
    QWidget *horizontalLayoutWidget_2;
    QHBoxLayout *horizontalLayout;
    QLabel *colorImage;
    QFrame *vLine;
    QLabel *depthImage;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QMenu *menuKinect;
    QMenu *menuStream;
    QMenu *menuDetection;
    QMenu *menuSegmentation;
    QMenu *menuTracking;
    QMenu *menuSettings;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *PeopleDetectorClass)
    {
        if (PeopleDetectorClass->objectName().isEmpty())
            PeopleDetectorClass->setObjectName(QStringLiteral("PeopleDetectorClass"));
        PeopleDetectorClass->resize(1300, 600);
        PeopleDetectorClass->setMinimumSize(QSize(300, 400));
        QIcon icon;
        icon.addFile(QStringLiteral("PeopleDetector.ico"), QSize(), QIcon::Normal, QIcon::Off);
        PeopleDetectorClass->setWindowIcon(icon);
        PeopleDetectorClass->setLocale(QLocale(QLocale::English, QLocale::UnitedStates));
        PeopleDetectorClass->setToolButtonStyle(Qt::ToolButtonIconOnly);
        actionConnect = new QAction(PeopleDetectorClass);
        actionConnect->setObjectName(QStringLiteral("actionConnect"));
        actionConnect->setCheckable(false);
        actionChange_Pitch = new QAction(PeopleDetectorClass);
        actionChange_Pitch->setObjectName(QStringLiteral("actionChange_Pitch"));
        actionChange_Pitch->setEnabled(false);
        actionFrom_Kinect = new QAction(PeopleDetectorClass);
        actionFrom_Kinect->setObjectName(QStringLiteral("actionFrom_Kinect"));
        actionFrom_Kinect->setCheckable(true);
        actionFrom_Kinect->setChecked(true);
        actionFrom_Kinect->setPriority(QAction::NormalPriority);
        actionFrom_File = new QAction(PeopleDetectorClass);
        actionFrom_File->setObjectName(QStringLiteral("actionFrom_File"));
        actionFrom_File->setCheckable(true);
        actionFrom_File->setPriority(QAction::NormalPriority);
        actionOpen_File = new QAction(PeopleDetectorClass);
        actionOpen_File->setObjectName(QStringLiteral("actionOpen_File"));
        actionExit = new QAction(PeopleDetectorClass);
        actionExit->setObjectName(QStringLiteral("actionExit"));
        actionSource_folder = new QAction(PeopleDetectorClass);
        actionSource_folder->setObjectName(QStringLiteral("actionSource_folder"));
        actionSource_folder->setEnabled(false);
        actionOutput_folder = new QAction(PeopleDetectorClass);
        actionOutput_folder->setObjectName(QStringLiteral("actionOutput_folder"));
        actionOutput_folder->setEnabled(false);
        actionThreshold = new QAction(PeopleDetectorClass);
        actionThreshold->setObjectName(QStringLiteral("actionThreshold"));
        actionThreshold->setCheckable(true);
        actionThreshold->setChecked(false);
        actionThreshold_OTSU = new QAction(PeopleDetectorClass);
        actionThreshold_OTSU->setObjectName(QStringLiteral("actionThreshold_OTSU"));
        actionThreshold_OTSU->setCheckable(true);
        actionThreshold_OTSU->setEnabled(true);
        actionReconstruction = new QAction(PeopleDetectorClass);
        actionReconstruction->setObjectName(QStringLiteral("actionReconstruction"));
        actionReconstruction->setCheckable(true);
        actionReconstruction->setChecked(true);
        actionReconstruction->setEnabled(true);
        actionThreshold_Adaptive = new QAction(PeopleDetectorClass);
        actionThreshold_Adaptive->setObjectName(QStringLiteral("actionThreshold_Adaptive"));
        actionThreshold_Adaptive->setCheckable(true);
        actionLucas_Kanade = new QAction(PeopleDetectorClass);
        actionLucas_Kanade->setObjectName(QStringLiteral("actionLucas_Kanade"));
        actionLucas_Kanade->setCheckable(true);
        actionLucas_Kanade->setChecked(false);
        actionFarneback = new QAction(PeopleDetectorClass);
        actionFarneback->setObjectName(QStringLiteral("actionFarneback"));
        actionFarneback->setCheckable(true);
        actionSimpleBlob = new QAction(PeopleDetectorClass);
        actionSimpleBlob->setObjectName(QStringLiteral("actionSimpleBlob"));
        actionSimpleBlob->setCheckable(true);
        actionSimpleBlob->setChecked(true);
        actionReset_Counters = new QAction(PeopleDetectorClass);
        actionReset_Counters->setObjectName(QStringLiteral("actionReset_Counters"));
        actionSetPeopleCounter = new QAction(PeopleDetectorClass);
        actionSetPeopleCounter->setObjectName(QStringLiteral("actionSetPeopleCounter"));
        actionDataPath = new QAction(PeopleDetectorClass);
        actionDataPath->setObjectName(QStringLiteral("actionDataPath"));
        centralWidget = new QWidget(PeopleDetectorClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        horizontalLayoutWidget = new QWidget(centralWidget);
        horizontalLayoutWidget->setObjectName(QStringLiteral("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(10, 0, 1281, 41));
        toolBox = new QHBoxLayout(horizontalLayoutWidget);
        toolBox->setSpacing(20);
        toolBox->setContentsMargins(11, 11, 11, 11);
        toolBox->setObjectName(QStringLiteral("toolBox"));
        toolBox->setSizeConstraint(QLayout::SetDefaultConstraint);
        toolBox->setContentsMargins(0, 0, 0, 0);
        playPshBtn = new QPushButton(horizontalLayoutWidget);
        playPshBtn->setObjectName(QStringLiteral("playPshBtn"));
        playPshBtn->setEnabled(false);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Maximum);
        sizePolicy.setHorizontalStretch(32);
        sizePolicy.setVerticalStretch(32);
        sizePolicy.setHeightForWidth(playPshBtn->sizePolicy().hasHeightForWidth());
        playPshBtn->setSizePolicy(sizePolicy);
        playPshBtn->setMinimumSize(QSize(32, 32));
        playPshBtn->setSizeIncrement(QSize(0, 0));
        playPshBtn->setBaseSize(QSize(0, 0));
        QIcon icon1;
        icon1.addFile(QStringLiteral(":/PeopleDetector/Resources/play.png"), QSize(), QIcon::Normal, QIcon::Off);
        icon1.addFile(QStringLiteral(":/PeopleDetector/Resources/pause.png"), QSize(), QIcon::Active, QIcon::On);
        playPshBtn->setIcon(icon1);
        playPshBtn->setCheckable(true);
        playPshBtn->setChecked(false);

        toolBox->addWidget(playPshBtn);

        detectPshBtn = new QPushButton(horizontalLayoutWidget);
        detectPshBtn->setObjectName(QStringLiteral("detectPshBtn"));
        detectPshBtn->setEnabled(false);
        sizePolicy.setHeightForWidth(detectPshBtn->sizePolicy().hasHeightForWidth());
        detectPshBtn->setSizePolicy(sizePolicy);
        detectPshBtn->setMinimumSize(QSize(32, 32));
        detectPshBtn->setSizeIncrement(QSize(0, 0));
        detectPshBtn->setBaseSize(QSize(0, 0));
        QIcon icon2;
        icon2.addFile(QStringLiteral(":/PeopleDetector/Resources/track.png"), QSize(), QIcon::Normal, QIcon::Off);
        icon2.addFile(QStringLiteral(":/PeopleDetector/Resources/tracking.png"), QSize(), QIcon::Normal, QIcon::On);
        detectPshBtn->setIcon(icon2);
        detectPshBtn->setCheckable(true);

        toolBox->addWidget(detectPshBtn);

        threshLbl = new QLabel(horizontalLayoutWidget);
        threshLbl->setObjectName(QStringLiteral("threshLbl"));

        toolBox->addWidget(threshLbl);

        threshVal = new QSpinBox(horizontalLayoutWidget);
        threshVal->setObjectName(QStringLiteral("threshVal"));
        threshVal->setEnabled(false);
        threshVal->setMaximum(40000);
        threshVal->setDisplayIntegerBase(10);

        toolBox->addWidget(threshVal);

        recordPshBtn = new QPushButton(horizontalLayoutWidget);
        recordPshBtn->setObjectName(QStringLiteral("recordPshBtn"));
        recordPshBtn->setEnabled(false);
        sizePolicy.setHeightForWidth(recordPshBtn->sizePolicy().hasHeightForWidth());
        recordPshBtn->setSizePolicy(sizePolicy);
        recordPshBtn->setMinimumSize(QSize(32, 32));
        recordPshBtn->setSizeIncrement(QSize(0, 0));
        recordPshBtn->setBaseSize(QSize(0, 0));
        QIcon icon3;
        icon3.addFile(QStringLiteral(":/PeopleDetector/Resources/record.png"), QSize(), QIcon::Normal, QIcon::Off);
        icon3.addFile(QStringLiteral(":/PeopleDetector/Resources/recording.png"), QSize(), QIcon::Normal, QIcon::On);
        recordPshBtn->setIcon(icon3);
        recordPshBtn->setCheckable(true);
        recordPshBtn->setAutoRepeat(false);

        toolBox->addWidget(recordPshBtn);

        outputLbl = new QLabel(horizontalLayoutWidget);
        outputLbl->setObjectName(QStringLiteral("outputLbl"));

        toolBox->addWidget(outputLbl);

        outputPath = new QLineEdit(horizontalLayoutWidget);
        outputPath->setObjectName(QStringLiteral("outputPath"));
        outputPath->setEnabled(false);
        QSizePolicy sizePolicy1(QSizePolicy::Maximum, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(outputPath->sizePolicy().hasHeightForWidth());
        outputPath->setSizePolicy(sizePolicy1);
        outputPath->setMinimumSize(QSize(600, 20));
        outputPath->setBaseSize(QSize(0, 0));
        outputPath->setFrame(true);
        outputPath->setReadOnly(false);
        outputPath->setClearButtonEnabled(false);

        toolBox->addWidget(outputPath);

        openFilePshBtn = new QPushButton(horizontalLayoutWidget);
        openFilePshBtn->setObjectName(QStringLiteral("openFilePshBtn"));
        openFilePshBtn->setEnabled(false);

        toolBox->addWidget(openFilePshBtn);

        counterLbl = new QLabel(horizontalLayoutWidget);
        counterLbl->setObjectName(QStringLiteral("counterLbl"));

        toolBox->addWidget(counterLbl);

        threshLbl->raise();
        playPshBtn->raise();
        detectPshBtn->raise();
        outputLbl->raise();
        outputPath->raise();
        openFilePshBtn->raise();
        counterLbl->raise();
        threshVal->raise();
        recordPshBtn->raise();
        horizontalLayoutWidget_2 = new QWidget(centralWidget);
        horizontalLayoutWidget_2->setObjectName(QStringLiteral("horizontalLayoutWidget_2"));
        horizontalLayoutWidget_2->setGeometry(QRect(10, 40, 1281, 511));
        horizontalLayout = new QHBoxLayout(horizontalLayoutWidget_2);
        horizontalLayout->setSpacing(5);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setSizeConstraint(QLayout::SetDefaultConstraint);
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        colorImage = new QLabel(horizontalLayoutWidget_2);
        colorImage->setObjectName(QStringLiteral("colorImage"));
        colorImage->setEnabled(true);
        QSizePolicy sizePolicy2(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(colorImage->sizePolicy().hasHeightForWidth());
        colorImage->setSizePolicy(sizePolicy2);
        colorImage->setMaximumSize(QSize(640, 480));
        colorImage->setFrameShape(QFrame::Box);
        colorImage->setFrameShadow(QFrame::Plain);
        colorImage->setLineWidth(1);
        colorImage->setAlignment(Qt::AlignCenter);

        horizontalLayout->addWidget(colorImage);

        vLine = new QFrame(horizontalLayoutWidget_2);
        vLine->setObjectName(QStringLiteral("vLine"));
        vLine->setFrameShape(QFrame::VLine);
        vLine->setFrameShadow(QFrame::Sunken);

        horizontalLayout->addWidget(vLine);

        depthImage = new QLabel(horizontalLayoutWidget_2);
        depthImage->setObjectName(QStringLiteral("depthImage"));
        depthImage->setEnabled(true);
        sizePolicy2.setHeightForWidth(depthImage->sizePolicy().hasHeightForWidth());
        depthImage->setSizePolicy(sizePolicy2);
        depthImage->setMaximumSize(QSize(640, 480));
        depthImage->setFrameShape(QFrame::Box);
        depthImage->setFrameShadow(QFrame::Plain);
        depthImage->setLineWidth(1);
        depthImage->setAlignment(Qt::AlignCenter);

        horizontalLayout->addWidget(depthImage);

        PeopleDetectorClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(PeopleDetectorClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1300, 21));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QStringLiteral("menuFile"));
        menuKinect = new QMenu(menuBar);
        menuKinect->setObjectName(QStringLiteral("menuKinect"));
        menuStream = new QMenu(menuBar);
        menuStream->setObjectName(QStringLiteral("menuStream"));
        menuDetection = new QMenu(menuBar);
        menuDetection->setObjectName(QStringLiteral("menuDetection"));
        menuSegmentation = new QMenu(menuDetection);
        menuSegmentation->setObjectName(QStringLiteral("menuSegmentation"));
        menuTracking = new QMenu(menuDetection);
        menuTracking->setObjectName(QStringLiteral("menuTracking"));
        menuSettings = new QMenu(menuBar);
        menuSettings->setObjectName(QStringLiteral("menuSettings"));
        PeopleDetectorClass->setMenuBar(menuBar);
        statusBar = new QStatusBar(PeopleDetectorClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        PeopleDetectorClass->setStatusBar(statusBar);

        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuKinect->menuAction());
        menuBar->addAction(menuStream->menuAction());
        menuBar->addAction(menuDetection->menuAction());
        menuBar->addAction(menuSettings->menuAction());
        menuFile->addAction(actionExit);
        menuKinect->addAction(actionConnect);
        menuKinect->addAction(actionChange_Pitch);
        menuStream->addAction(actionFrom_Kinect);
        menuStream->addAction(actionFrom_File);
        menuStream->addSeparator();
        menuStream->addAction(actionSource_folder);
        menuStream->addAction(actionOutput_folder);
        menuDetection->addAction(menuSegmentation->menuAction());
        menuDetection->addAction(menuTracking->menuAction());
        menuDetection->addSeparator();
        menuDetection->addAction(actionReset_Counters);
        menuSegmentation->addAction(actionReconstruction);
        menuSegmentation->addAction(actionThreshold);
        menuSegmentation->addAction(actionThreshold_OTSU);
        menuSegmentation->addAction(actionThreshold_Adaptive);
        menuTracking->addAction(actionSimpleBlob);
        menuTracking->addAction(actionLucas_Kanade);
        menuTracking->addAction(actionFarneback);
        menuSettings->addAction(actionSetPeopleCounter);
        menuSettings->addAction(actionDataPath);

        retranslateUi(PeopleDetectorClass);
        QObject::connect(actionExit, SIGNAL(triggered()), PeopleDetectorClass, SLOT(exitProgram()));
        QObject::connect(actionChange_Pitch, SIGNAL(triggered()), PeopleDetectorClass, SLOT(changeKinectPitch()));
        QObject::connect(actionFrom_File, SIGNAL(triggered(bool)), PeopleDetectorClass, SLOT(fileStreamActivate(bool)));
        QObject::connect(actionFrom_Kinect, SIGNAL(triggered()), PeopleDetectorClass, SLOT(kinectStreamActivate()));
        QObject::connect(actionConnect, SIGNAL(triggered()), PeopleDetectorClass, SLOT(connectKinect()));
        QObject::connect(actionSource_folder, SIGNAL(triggered()), PeopleDetectorClass, SLOT(openSourceFolder()));
        QObject::connect(actionOutput_folder, SIGNAL(triggered()), PeopleDetectorClass, SLOT(openOutputFolder()));
        QObject::connect(actionThreshold, SIGNAL(triggered()), PeopleDetectorClass, SLOT(actionThreshold()));
        QObject::connect(actionThreshold_OTSU, SIGNAL(triggered()), PeopleDetectorClass, SLOT(actionThresholdOTSU()));
        QObject::connect(actionThreshold_Adaptive, SIGNAL(triggered()), PeopleDetectorClass, SLOT(actionAdaptiveThreshold()));
        QObject::connect(actionReconstruction, SIGNAL(triggered()), PeopleDetectorClass, SLOT(actionReconstruction()));
        QObject::connect(actionLucas_Kanade, SIGNAL(triggered()), PeopleDetectorClass, SLOT(actionLucas_Kanade()));
        QObject::connect(actionSimpleBlob, SIGNAL(triggered()), PeopleDetectorClass, SLOT(actionSimpleBlob()));
        QObject::connect(actionFarneback, SIGNAL(triggered()), PeopleDetectorClass, SLOT(actionFarneback()));
        QObject::connect(actionReset_Counters, SIGNAL(triggered()), PeopleDetectorClass, SLOT(actionResetCounters()));
        QObject::connect(actionSetPeopleCounter, SIGNAL(triggered()), PeopleDetectorClass, SLOT(actionPeopleCounterSettings()));
        QObject::connect(actionDataPath, SIGNAL(triggered()), PeopleDetectorClass, SLOT(actionSetDataPath()));

        QMetaObject::connectSlotsByName(PeopleDetectorClass);
    } // setupUi

    void retranslateUi(QMainWindow *PeopleDetectorClass)
    {
        PeopleDetectorClass->setWindowTitle(QApplication::translate("PeopleDetectorClass", "PeopleDetector", 0));
        actionConnect->setText(QApplication::translate("PeopleDetectorClass", "Connect", 0));
#ifndef QT_NO_TOOLTIP
        actionConnect->setToolTip(QApplication::translate("PeopleDetectorClass", "Establishes connection to active Kinect sensor", 0));
#endif // QT_NO_TOOLTIP
        actionConnect->setShortcut(QApplication::translate("PeopleDetectorClass", "Ctrl+T", 0));
        actionChange_Pitch->setText(QApplication::translate("PeopleDetectorClass", "Change Pitch", 0));
#ifndef QT_NO_TOOLTIP
        actionChange_Pitch->setToolTip(QApplication::translate("PeopleDetectorClass", "Set pitch of Kinect sensor", 0));
#endif // QT_NO_TOOLTIP
        actionFrom_Kinect->setText(QApplication::translate("PeopleDetectorClass", "From Kinect", 0));
#ifndef QT_NO_TOOLTIP
        actionFrom_Kinect->setToolTip(QApplication::translate("PeopleDetectorClass", "Sets default reading stream to Kinect sensor stream", 0));
#endif // QT_NO_TOOLTIP
        actionFrom_File->setText(QApplication::translate("PeopleDetectorClass", "From File", 0));
#ifndef QT_NO_TOOLTIP
        actionFrom_File->setToolTip(QApplication::translate("PeopleDetectorClass", "Sets default reading stream to directory", 0));
#endif // QT_NO_TOOLTIP
        actionOpen_File->setText(QApplication::translate("PeopleDetectorClass", "Open File", 0));
        actionExit->setText(QApplication::translate("PeopleDetectorClass", "Exit", 0));
#ifndef QT_NO_TOOLTIP
        actionExit->setToolTip(QApplication::translate("PeopleDetectorClass", "Exits the program properly", 0));
#endif // QT_NO_TOOLTIP
        actionExit->setShortcut(QApplication::translate("PeopleDetectorClass", "Ctrl+Q", 0));
        actionSource_folder->setText(QApplication::translate("PeopleDetectorClass", "Source folder", 0));
        actionSource_folder->setShortcut(QApplication::translate("PeopleDetectorClass", "Ctrl+I", 0));
        actionOutput_folder->setText(QApplication::translate("PeopleDetectorClass", "Output folder", 0));
        actionOutput_folder->setShortcut(QApplication::translate("PeopleDetectorClass", "Ctrl+O", 0));
        actionThreshold->setText(QApplication::translate("PeopleDetectorClass", "Threshold", 0));
        actionThreshold_OTSU->setText(QApplication::translate("PeopleDetectorClass", "Threshold OTSU", 0));
        actionReconstruction->setText(QApplication::translate("PeopleDetectorClass", "M. Reconstruction", 0));
        actionThreshold_Adaptive->setText(QApplication::translate("PeopleDetectorClass", "Threshold Adaptive", 0));
        actionLucas_Kanade->setText(QApplication::translate("PeopleDetectorClass", "Lucas-Kanade", 0));
        actionFarneback->setText(QApplication::translate("PeopleDetectorClass", "Farneback", 0));
        actionSimpleBlob->setText(QApplication::translate("PeopleDetectorClass", "SimpleBlobDetector", 0));
        actionReset_Counters->setText(QApplication::translate("PeopleDetectorClass", "Reset Counters", 0));
        actionSetPeopleCounter->setText(QApplication::translate("PeopleDetectorClass", "Counter Settings", 0));
        actionSetPeopleCounter->setShortcut(QApplication::translate("PeopleDetectorClass", "Ctrl+T", 0));
        actionDataPath->setText(QApplication::translate("PeopleDetectorClass", "Export Data Path", 0));
        actionDataPath->setShortcut(QApplication::translate("PeopleDetectorClass", "Ctrl+E", 0));
        playPshBtn->setText(QString());
        playPshBtn->setShortcut(QApplication::translate("PeopleDetectorClass", "Ctrl+P", 0));
        detectPshBtn->setText(QString());
        detectPshBtn->setShortcut(QApplication::translate("PeopleDetectorClass", "Ctrl+D", 0));
        threshLbl->setText(QApplication::translate("PeopleDetectorClass", "Min. human height (mm)", 0));
        recordPshBtn->setText(QString());
        outputLbl->setText(QApplication::translate("PeopleDetectorClass", "Output:", 0));
        outputPath->setInputMask(QString());
        outputPath->setPlaceholderText(QString());
        openFilePshBtn->setText(QApplication::translate("PeopleDetectorClass", "Open", 0));
        counterLbl->setText(QString());
        colorImage->setText(QApplication::translate("PeopleDetectorClass", "ColorMap", 0));
        depthImage->setText(QApplication::translate("PeopleDetectorClass", "DepthMap", 0));
        menuFile->setTitle(QApplication::translate("PeopleDetectorClass", "File", 0));
        menuKinect->setTitle(QApplication::translate("PeopleDetectorClass", "Kinect", 0));
        menuStream->setTitle(QApplication::translate("PeopleDetectorClass", "Stream", 0));
        menuDetection->setTitle(QApplication::translate("PeopleDetectorClass", "Detection", 0));
        menuSegmentation->setTitle(QApplication::translate("PeopleDetectorClass", "Segmentation", 0));
        menuTracking->setTitle(QApplication::translate("PeopleDetectorClass", "Tracking", 0));
        menuSettings->setTitle(QApplication::translate("PeopleDetectorClass", "Settings", 0));
    } // retranslateUi

};

namespace Ui {
    class PeopleDetectorClass: public Ui_PeopleDetectorClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PEOPLEDETECTOR_H
