#include "stdafx.h"
#include "KinectTiltAngleManager.h"

inline LONG CoerceElevationAngle(LONG tiltAngle)
{
    return min(max(NUI_CAMERA_ELEVATION_MINIMUM, tiltAngle), NUI_CAMERA_ELEVATION_MAXIMUM);
}

KinectTiltAngleManager::KinectTiltAngleManager() : 
	m_pSensor(NULL),	
	m_tiltAngle(LONG_MAX),  
	m_hElevationTaskThread(nullptr)
{
}

KinectTiltAngleManager::~KinectTiltAngleManager(void)
{
	// Exit the elevation task thread
    SetEvent(m_hExitThreadEvent);

    // Wait for the elevation task thread
    WaitForSingleObject(m_hElevationTaskThread, INFINITE);

    SafeRelease(m_pSensor);

    if (m_hElevationTaskThread)
    {
        CloseHandle(m_hElevationTaskThread);
    }
}

void KinectTiltAngleManager::Initialize(INuiSensor *pSensor)
{
	if (!m_pSensor)
	{
		m_pSensor = pSensor;
		m_pSensor->AddRef();
	}

	// Start the elevation task thread
	StartElevationThread();
}

bool KinectTiltAngleManager::IsInitialized()
{
	return m_pSensor != NULL;
}


void KinectTiltAngleManager::GetElevationAngle(LONG *angle)
{
	LONG tmpAngle = 0;
    if (SUCCEEDED(m_pSensor->NuiCameraElevationGetAngle(&tmpAngle)))
    {
        CompareUpdateValue(CoerceElevationAngle(tmpAngle), m_tiltAngle);
		*angle = m_tiltAngle;
    }
}


void KinectTiltAngleManager::SetElevationAngle(LONG angle)
{
	CompareUpdateValue(CoerceElevationAngle(angle), m_tiltAngle);
	
    // Resume the elevation task thread to set tilt angle
    SetEvent(m_hSetTiltAngleEvent);
}


void KinectTiltAngleManager::StartElevationThread(void)
{
	// Create the events for elevation task thread
    m_hSetTiltAngleEvent = CreateEventW(nullptr, FALSE, FALSE, nullptr);
    m_hExitThreadEvent = CreateEventW(nullptr, TRUE, FALSE, nullptr);

    m_hElevationTaskThread = CreateThread(
        nullptr,
        0,
		(LPTHREAD_START_ROUTINE)KinectTiltAngleManager::ElevationThread,
        (LPVOID)this,
        0,
        nullptr);
}


DWORD WINAPI KinectTiltAngleManager::ElevationThread(KinectTiltAngleManager *pThis)
{
	const int numEvents = 2;
    HANDLE events[numEvents] = {pThis->m_hSetTiltAngleEvent, pThis->m_hExitThreadEvent};

    while(true)
    {
        // Check if we have a setting tilt angle event or an exiting thread event
        DWORD dwEvent = WaitForMultipleObjects(numEvents, events, FALSE, INFINITE);

        if (WAIT_OBJECT_0 == dwEvent)
        {
            // Set the tilt angle
            pThis->m_pSensor->NuiCameraElevationSetAngle(pThis->m_tiltAngle);
        }
        else if (WAIT_OBJECT_0 + 1 == dwEvent)
        {
            // Close the handles and exit the thread
            CloseHandle(pThis->m_hSetTiltAngleEvent);
            CloseHandle(pThis->m_hExitThreadEvent);

            break;
        }
        else
        {
            return 1;
        }
    }

    return 0;
}
