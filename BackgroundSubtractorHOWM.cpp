#include "stdafx.h"
#include "BackgroundSubtractorHOWM.h"


BackgroundSubtractorHOWM::BackgroundSubtractorHOWM(int noiseThresh) :
	m_history(0),
	m_noiseThreshold(noiseThresh)
{
}


BackgroundSubtractorHOWM::~BackgroundSubtractorHOWM(void)
{
}


void BackgroundSubtractorHOWM::operator()(cv::Mat image, cv::Mat &fgmask, double learningRate)
{
	fgmask = cv::Mat::zeros(image.rows,image.cols,CV_8U);
	if (m_history == 0)
	{
		image.copyTo(m_MatrixOfMedians);
		m_BackgroundImage = cv::Mat(image.rows,image.cols,CV_8U);
		m_history++;
		return;
	}

	cv::addWeighted(m_MatrixOfMedians,1-learningRate,image,learningRate,0,m_MatrixOfMedians);
	
	//if ( image.type() == CV_8U )
	//{
	//	for (UINT y = 0; y < image.rows; y++)
	//	{
	//		// Get row pointer for depth Mat
	//		BYTE* pImageRow = image.ptr<BYTE>(y);
	//		BYTE* pFgMaskRow = fgmask.ptr<BYTE>(y);
	//		BYTE* pMediansRow = m_MatrixOfMedians.ptr<BYTE>(y);

	//		for (UINT x = 0; x < image.cols; x++)
	//		{
	//			BYTE diff = pImageRow[x] - pMediansRow[x] <= 0 ? 0 : pImageRow[x] - pMediansRow[x];
	//			diff = diff <= m_noiseThreshold ? 0 : 255;
	//			pFgMaskRow[x] = diff;
	//		}
	//	}
	//}
	//if ( image.type() == CV_16U )
	//{
	//	for (UINT y = 0; y < image.rows; y++)
	//	{
	//		// Get row pointer for depth Mat
	//		USHORT* pImageRow = image.ptr<USHORT>(y);
	//		BYTE* pFgMaskRow = fgmask.ptr<BYTE>(y);
	//		USHORT* pMediansRow = m_MatrixOfMedians.ptr<USHORT>(y);
	//		USHORT diff;
	//		for (UINT x = 0; x < image.cols; x++)
	//		{
	//			diff = pImageRow[x] - pMediansRow[x] <= 0 ? 0 : pImageRow[x] - pMediansRow[x];
	//			diff = diff <= m_noiseThreshold ? 0 : 255;
	//			pFgMaskRow[x] = static_cast<BYTE>(diff);
	//		}
	//	}
	//}

	cv::Mat diff = image - m_MatrixOfMedians;
	diff.convertTo(fgmask, CV_8U);
	cv::threshold(fgmask,fgmask,m_noiseThreshold,255,CV_THRESH_BINARY);
	
	cv::threshold(fgmask,m_BackgroundImage,1,255,CV_THRESH_BINARY_INV);
	m_history++;

}


void BackgroundSubtractorHOWM::getBackgroundImage(cv::Mat &backgroundImage) const
{
	m_BackgroundImage.copyTo(backgroundImage);
}


void BackgroundSubtractorHOWM::getMediansMatrix(cv::Mat &medianMatrix)
{
	m_MatrixOfMedians.copyTo(medianMatrix);
}


void BackgroundSubtractorHOWM::initialize(int noiseThresh)
{
	m_noiseThreshold = noiseThresh;
	m_history = 0;
}