#include <opencv2\opencv.hpp>
#include <opencv2\core\core.hpp>

#pragma once

using namespace cv;
class Person
{
public:

	enum Directions
	{
		// default for the one in image not coming in or out
		STAYING,
		// if the one is going in the monitored zone
		VISITED,
		// if the one left monitoring zone and entered area
		ENTERED,
		// if the one is leaving the monitored area to monitored zone
		LEAVING,
		// if the one just left the monitored zone and the whole faction
		LEFT,
		// if the state according to position is unknown
		UNKNOWN
	};

	Person(void);
	Person(Point2f position, Scalar color, Directions state = Directions::STAYING);
	~Person(void);

	Scalar GetLineColor();
	double SetNextPosition(Point2f nextPosition);
	void SetDirectionState(Directions direction);
	int GetDirectionState();
	Point2f GetPreviousPosition();
	Point2f GetActualPosition();

private :
	
	int m_Direction;
	Scalar m_Color;
	Point2f m_PreviousPosition;
	Point2f m_ActualPosition;
};

