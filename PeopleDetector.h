#ifndef PEOPLEDETECTOR_H
#define PEOPLEDETECTOR_H

#include <QtWidgets/QMainWindow>
#include <opencv2\core\core.hpp>
#include <opencv2\opencv.hpp>
#include "ui_PeopleDetector.h"
#include "KinectPitch.h"
#include "PeopleCounterSettings.h"
#include "DetectionManager.h"
#include "KinectController.h"
#include "FileStreamManager.h"

using namespace cv;
class PeopleDetector : public QMainWindow
{
	Q_OBJECT

public:
	PeopleDetector(QWidget *parent = 0);
	~PeopleDetector();

private slots :
	void on_playPshBtn_clicked();
	void on_recordPshBtn_clicked();
	void on_openFilePshBtn_clicked();
	void on_detectPshBtn_clicked();
	
	void exitProgram();
	
	void connectKinect();
	void changeKinectPitch();

	void kinectStreamActivate();
	void fileStreamActivate(bool checked);

	void openSourceFolder();
	void openOutputFolder();

	void actionThreshold();
	void actionThresholdOTSU();
	void actionAdaptiveThreshold();
	void actionReconstruction();

	void actionLucas_Kanade();
	void actionSimpleBlob();
	void actionFarneback();

	void actionResetCounters();
	void actionPeopleCounterSettings();
	void actionSetDataPath();

public slots :
	void showImages(Mat *colorMat, Mat *depthMat, long counter);
	void fileStreamStatus(QString text);
	void setKinectPitch(int angle);

private:
	Ui::PeopleDetectorClass ui;
	KinectPitch m_PitchDialog;
	PeopleCounterSettings m_CounterSettingsDialog;

	KinectController m_KinectController;
	DetectionManager m_DetectionManager;
	FileStreamManager m_FileManager;

	QActionGroup *m_pStreamCheckerGroup;
	QActionGroup *m_pSegmentationMethodGroup;
	QActionGroup *m_pTrackingMethodGroup;

	QMutex guiMutex;

	QImage MatToQImage(Mat src);
};

#endif // PEOPLEDETECTOR_H
