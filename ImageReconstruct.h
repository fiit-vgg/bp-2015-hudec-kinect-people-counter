#ifndef _IMAGERECONSTRUCTION_H_
#define _IMAGERECONSTRUCTION_H_

#include ".\opencv2\OpenCv.hpp"
//#include "highgui.h"
//#include "cv.h"
//#include "cvcompat.h"
#include "Queue.h"

//    image - input/output raster image that must have 8uC1 or 8sC1 format.

//



int ImReconstruct( IplImage* oMarkerImage, IplImage* oMaskImage);
void PropagationStep(unsigned char*pPixelMarker, unsigned char*pPixelMask ,int nX, int nY, int nOffsetX, int nOffsetY, int nStep,  XYQueueC & xyQueue);
void PropagationStep16(unsigned short*pPixelMarker, unsigned short*pPixelMask ,int nX, int nY, int nOffsetX, int nOffsetY, int nStep,  XYQueueC & xyQueue);

#endif/*_IMAGERECONSTRUCTION_H_*/
